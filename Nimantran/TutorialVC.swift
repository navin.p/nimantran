//
//  TutorialVC.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/5/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class TutorialVC: UIViewController {
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var collectionForWelcome: UICollectionView!
    @IBOutlet weak var pageControll: UIPageControl!

    
    //MARK:
    //MARK:- CustomeVariable
    let  ary_CollectionData = [["title":"Bonus Features ","subtitle" :"With e-invites you can save paper in huge quantities and eventually, save trees.","image":"intro_1"],["title":"Experience Convenience","subtitle" :"With e-invites you can save paper in huge quantities and eventually, save trees.","image":"intro_2"],["title":"Save Paper","subtitle" :"With e-invites you can save paper in huge quantities and eventually, save trees.","image":"intro_2"]]
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
               
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
  
       
    // MARK:
    // MARK:- IBAction
   
    
    
}
// MARK:
// MARK:- UICollectionView
extension TutorialVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCell", for: indexPath as IndexPath) as! welcomeCell
        let dict = ary_CollectionData[indexPath.row]as NSDictionary
        cell.welcome_lbl_Title.text = dict["title"]as? String
        cell.welcome_lbl_SubTitle.text = dict["subtitle"]as? String
        cell.welcome_Image.image = UIImage(named: dict["image"]as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionForWelcome.frame.size.width) , height:(self.collectionForWelcome.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControll.currentPage = indexPath.row
    }

    
}
class welcomeCell: UICollectionViewCell {
    @IBOutlet weak var welcome_Image: UIImageView!
    @IBOutlet weak var welcome_lbl_Title: UILabel!
    @IBOutlet weak var welcome_lbl_SubTitle: UILabel!
}
