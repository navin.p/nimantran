//
//  ImportExportVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/26/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import Alamofire

import TransitionButton
import CRNotifications

class ImportExportVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var btnImport: TransitionButton!
    @IBOutlet weak var btnExport: TransitionButton!

    var fileName = ""
    // MARK:
     // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
         for item in self.btnImport.layer.sublayers! {
             if item is CAGradientLayer {
                 item.removeFromSuperlayer()
             }
         }
         btnImport.layer.insertSublayer(getgradientLayer(bounds: btnImport.bounds), at: 0)
    for item in self.btnExport.layer.sublayers! {
               if item is CAGradientLayer {
                   item.removeFromSuperlayer()
               }
           }
        btnExport.layer.insertSublayer(getgradientLayer(bounds: btnExport.bounds), at: 0)
        
    }
      // MARK:
      // MARK:- IBAction
      @IBAction func actionOnBack(_ sender: UIButton) {
          self.view.endEditing(true)
       self.navigationController?.popViewController(animated: true)
      }
    @IBAction func actionOnImport(_ sender: UIButton) {
        let url = URL(string: URL_address_book_format)
        loadFileAsync(url: url!)
        }

    

   @IBAction func actionOnExport(_ sender: UIButton) {
    let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)

     documentPicker.delegate = self
     present(documentPicker, animated: true, completion: nil)
    
          }
    
   
    func sharePDF() {

        let fm = FileManager.default

        var pdfURL = (fm.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        pdfURL = pdfURL.appendingPathComponent("address_book_format.xlsx") as URL

        //Rename document name to "Hello.pdf"
        let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("youraddress_book_format.xlsx") as NSURL

        do {
            let data = try Data(contentsOf: pdfURL)

            try data.write(to: url as URL)

            let activitycontroller = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            if activitycontroller.responds(to: #selector(getter: activitycontroller.completionWithItemsHandler))
            {
                activitycontroller.completionWithItemsHandler = {(type, isCompleted, items, error) in
                    if isCompleted
                    {
                        print("completed")
                    }
                }
            }
            DispatchQueue.main.async {
             activitycontroller.excludedActivityTypes = [UIActivity.ActivityType.airDrop]
                         activitycontroller.popoverPresentationController?.sourceView = self.view
                         self.present(activitycontroller, animated: true, completion: nil)
            }
           

        }
        catch {
            //ERROR
        }
    }
    func loadFileAsync(url: URL)
       {
  //  let loader = loader_Show(controller: self, strMessage: "Downloading...", title: "Please wait...", style: .actionSheet)

        
        
               let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
               var request = URLRequest(url: url)
               request.httpMethod = "GET"
               let task = session.dataTask(with: request, completionHandler:
               {
                   data, response, error in
           //     loader.dismiss(animated: false, completion: nil)
                   if error == nil
                   {
                       if let response = response as? HTTPURLResponse
                       {
                           if response.statusCode == 200
                           {
                               if let data = data
                               {
                                self.dataToFile(fileName: "address_book_format.xlsx", dataFile: data)
                               }
                               else
                               {
                               }
                           }
                       }
                   }
                   else
                   {
                   }
               })
               task.resume()
           }
    
    func dataToFile(fileName: String , dataFile : Data)  {

          // Make a constant from the data

          // Make the file path (with the filename) where the file will be loacated after it is created
          let filePath = getDocumentsDirectory().appendingPathComponent(fileName)

          do {
              // Write the file from data into the filepath (if there will be an error, the code jumps to the catch block below)
              try dataFile.write(to: URL(fileURLWithPath: filePath))
            self.sharePDF()

          } catch {
              // Prints the localized description of the error from the do block
              print("Error writing the file: \(error.localizedDescription)")
          }

          // Returns nil if there was an error in the do-catch -block
        //  return nil

      }

   
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func setUPSendData(url : URL , type : String) {
        
               
               self.fileName = getUniqueString() + "\(url.lastPathComponent)"
               do {
                   let weatherData = try NSData(contentsOf: url, options: NSData.ReadingOptions())

                
                let dict = ["FileName": "\(self.fileName)" , "AddressType": "\(type)", "HostUserId": "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)" ]
                               let encoder = JSONEncoder()
                               if let jsonData = try? encoder.encode(dict) {
                                   if let jsonString = String(data: jsonData, encoding: .utf8) {
                                       print(jsonString)
                                    
                                    self.APIExportFile(dict: jsonString, datFile: weatherData as Data, FileName: "\(self.fileName)")
                                       
                                   }
                               }
               
               } catch {
                          print(error)
                      }
               
              
               
               
        

               
               
               print(url.lastPathComponent)

                      print(url.pathExtension)
    }
    
}


extension ImportExportVC: UIDocumentPickerDelegate{


     func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        controller.dismiss(animated: true, completion: nil)
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
                                     let HomeAction = UIAlertAction(title: "Home", style: .default, handler:
                                     {
                                         (alert: UIAlertAction!) -> Void in
                                      
                                      self.setUPSendData(url: url, type: "Home")


                                     })

                                     let FriendAction = UIAlertAction(title: "Friend", style: .default, handler:
                                     {
                                         (alert: UIAlertAction!) -> Void in
                                        self.setUPSendData(url: url, type: "Friend")

                                     })

                                     let WorkAction = UIAlertAction(title: "Work", style: .default, handler:
                                     {
                                         (alert: UIAlertAction!) -> Void in
                                        self.setUPSendData(url: url, type: "Work")

                                     })
                                     let PersonalAction = UIAlertAction(title: "Personal", style: .default, handler:
                                           {
                                               (alert: UIAlertAction!) -> Void in
                                              self.setUPSendData(url: url, type: "Personal")

                                           })
        let OthersAction = UIAlertAction(title: "Others", style: .default, handler:
                                                 {
                                                     (alert: UIAlertAction!) -> Void in
                                                    self.setUPSendData(url: url, type: "Others")

                                                 })
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler:
                                                 {
                                                     (alert: UIAlertAction!) -> Void in

                                                 })
        optionMenu.addAction(HomeAction)
        optionMenu.addAction(FriendAction)
        optionMenu.addAction(WorkAction)
        optionMenu.addAction(PersonalAction)
        optionMenu.addAction(OthersAction)
        optionMenu.addAction(cancelAction)

        
        self.present(optionMenu, animated: true, completion: nil)
                                  
        
              }

    
  }
extension ImportExportVC  {
    
    func APIExportFile(dict : String , datFile : Data , FileName : String) {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Removing event...", title: "Please wait...", style: .actionSheet)

            WebServiceClass.getRequestWithHeadersWithFile(JsonData: dict,JsonDataKey: "ImportAddressBookMdl", dataFile: datFile, FileName: FileName, url: URL_ImportAddressBook) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if(status){
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if(apiStatus == "True"){
                      
                        CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 3)

                        for controller in self.navigationController!.viewControllers as Array {
                                                                                if controller.isKind(of: DashBoardVC.self) {
                                                                                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                                                                                    break
                                                                                }
                                                                            }
                        
                        
                    }else{
                                                      CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)

                    }
                }else{
                         CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                }
            }
          
            
        }
        else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
            
        }
    }
}
