//
//  DashBoardVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import Alamofire
import CRNotifications
import LoadingPlaceholderView
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

class DashBoardVC: UIViewController {
    
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var viewHold: UIView!
    
    @IBOutlet weak var txtSearch: UISearchBar!
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strErrorMessage  = ""
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    private var numberOfSections = 0
    private var numberOfRows = 0
    private var loadingPlaceholderView = LoadingPlaceholderView(){
        didSet{
            do {
                loadingPlaceholderView.gradientColor = .white
                loadingPlaceholderView.backgroundColor = .white
            }
        }
    }
    var refreshControl = UIRefreshControl()
    
    private var cellsIdentifiers = [
        "EventCell",
        "EventCell","EventCell",
        "EventCell","EventCell",
        "EventCell"
    ]
    
    
    
    
    
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getTokenFCM()
        txtSearch.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Get Event list...")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        if(isInternetAvailable()){
            loadingPlaceholderView.cover(viewHold, animated: true)
            getEventLiastByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
        }else{
            self.aryList = NSMutableArray()
            self.strErrorMessage = alertInternet
            self.tableView.reloadData()
        }
        tableView.addSubview(refreshControl)
        
        APIAddDeviceRegistration()
  
        //Hendel Notification When Application in active Mode
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "NIM_Notification"),
        object: nil,
        queue: nil,
        using:catchNotification)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    // MARK:
    // MARK:- Extra function
    
    @objc func refresh(sender:AnyObject) {
        
        
        txtSearch.text = ""
        getEventLiastByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
        
    }
    func catchNotification(notification:Notification) {
        
        let userInfo = notification.userInfo
        let dict = (userInfo as AnyObject)as! NSDictionary
        let aps = (dict.value(forKey: "aps")as! NSDictionary)
        let alert = (aps.value(forKey: "alert")as! NSDictionary)
        let strTitle = "\(alert.value(forKey: "title")!)"
        
        let strbody = "\(alert.value(forKey: "body")!)"
        
        let alertView = UIAlertController(title: strTitle, message: strbody, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
            vc!.dictNotiData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }))
        alertView.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
            
            
        }))
        self.present(alertView, animated: true, completion: nil)
    }
    
    func AddCoHostForEvent() {
        let alertVC = UIAlertController(title: "Add a Co-Host", message: "Enter event code", preferredStyle: .alert)
        alertVC.addTextField { (textField) in
            textField.placeholder = "Event code"
        }
        
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: {
            (alert) -> Void in
            
            let strEventCode = alertVC.textFields![0] as UITextField
            if(strEventCode.text != ""){
                self.JoinCoHostCode(CoHostCode: strEventCode.text!)
            }
            
        })
        let CancleAction = UIAlertAction(title: "Close", style: .destructive, handler: {
            (alert) -> Void in
            
        })
        alertVC.addAction(CancleAction)
        
        alertVC.addAction(submitAction)
        self.present(alertVC, animated: true)
    }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnMenu(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc: MenuVC = self.storyboard!.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDrawerView = self
        self.present(vc, animated: false, completion: {})
    }
    
    @IBAction func actionOnAddEvent(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddEventVC") as? AddEventVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    // MARK:
    // MARK:- API Calling
    func DeleteCoHostEvent(userID : String,eventID : String ,tagIndex : Int) {
        
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Removing event...", title: "Please wait...", style: .actionSheet)
            
            let urlForgotPass = "\(URL_GetDeleteEvent)EventId=\(eventID)"
            WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                loader.dismiss(animated: true, completion: nil)
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        CRNotifications.showNotification(type: CRNotifications.success, title:alertMsg , message: apiError, dismissDelay: 2)
                        self.arySelectedList.removeObject(at: tagIndex)
                        self.aryList.removeObject(at: tagIndex)
                        DispatchQueue.main.async {
                            self.numberOfSections = 1
                            self.numberOfRows = self.arySelectedList.count
                            self.tableView.reloadData()
                        }
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                    }
                }else{
                    
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    
    func getEventDelete(eventID : String ,tagIndex : Int) {
        
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Deleting event...", title: "Please wait...", style: .actionSheet)
            
            let urlForgotPass = "\(URL_GetDeleteEvent)EventId=\(eventID)"
            WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                loader.dismiss(animated: true, completion: nil)
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        CRNotifications.showNotification(type: CRNotifications.success, title:alertMsg , message: apiError, dismissDelay: 2)
                        self.arySelectedList.removeObject(at: tagIndex)
                        self.aryList.removeObject(at: tagIndex)
                        DispatchQueue.main.async {
                            self.numberOfSections = 1
                            self.numberOfRows = self.arySelectedList.count
                            self.tableView.reloadData()
                        }
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                    }
                }else{
                    
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    
    
    
    
    func getEventLiastByUserID(userID : String , tag : Int) {
        
        let dict = NSMutableDictionary()
        dict.setValue("0", forKey: "EventId")
        dict.setValue("\(userID)", forKey: "UserId")
        dict.setValue("0", forKey: "AddressBookId")
        dict.setValue("0", forKey: "SenderByUserId")
        dict.setValue("True", forKey: "IsActive")
        dict.setValue("0", forKey: "RecipientUserId")

        //  var jsoneString = getJson(from: dict)
        
        WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GetEvent) { (responce, status) in
            
            if(status){
                let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                if(apiStatus == "True"){
                    let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                    print("----------\(arryData)")
                    
                    self.aryList = NSMutableArray()
                    self.aryList = arryData.mutableCopy()as! NSMutableArray
                    self.arySelectedList = NSMutableArray()
                    self.arySelectedList = arryData.mutableCopy()as! NSMutableArray
                    self.strErrorMessage = ""
                }else{
                    self.arySelectedList = NSMutableArray()
                    self.aryList = NSMutableArray()
                    self.strErrorMessage = apiError
                }
            }else{
                self.arySelectedList = NSMutableArray()
                self.aryList = NSMutableArray()
                self.strErrorMessage = alertSomeError
            }
            
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.loadingPlaceholderView.uncover(animated: true)
                self.numberOfSections = 1
                self.numberOfRows = self.arySelectedList.count
                self.tableView.reloadData()
            }
        }
        
        
    }
    
    
    func APIAddDeviceRegistration() {
        if(isInternetAvailable()){
            if(nsud.value(forKey: "NIM_FCM_Token") != nil){
                let dict = NSMutableDictionary()
                
                
                dict.setValue("\(nsud.value(forKey: "NIM_FCM_Token")!)", forKey: "DeviceKey")
                dict.setValue("\(String(describing: UIDevice.current.identifierForVendor!.uuidString))", forKey: "ImeiNo")
                
                
                
                
                dict.setValue("\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", forKey: "UserId")
                
                
                WebServiceClass.getRequestWithHeaders(dictJson: dict, url: AddDeviceRegistration) { (responce, status) in
                    if(status){
                        print(responce)
                        let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                        //  let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                        if(apiStatus == "True"){
                            // CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 2)
                            
                        }else{
                            // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                            
                        }
                    }else{
                        // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                        
                    }
                    
                }
                
            }
            
            
            
            
            
        }
        
    }
    
    func GenerateCoHost(eventID : String ,tagIndex : Int) {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Generateing event code...", title: "Please wait...", style: .actionSheet)
            
            let dict = NSMutableDictionary()
            dict.setValue("0", forKey: "CoHostId")
            dict.setValue("\(eventID)", forKey: "EventId")
            WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GenerateCoHost) { (responce, status) in
                loader.dismiss(animated: false) {
                    if(status){
                        let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                        let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                        if(apiStatus == "True"){
                            
                            self.GetCoHost(eventID: eventID, tagIndex: tagIndex)
                        }else{
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                            
                        }
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                    }
                }
                
                
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    
    func GetCoHost(eventID : String ,tagIndex : Int) {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Geting event code..", title: "Please wait...", style: .actionSheet)
            
            let GetCoHost = "\(URL_GetCoHost)EventId=\(eventID)"
            WebServiceClass.getRequestWithHeadersGET(url: GetCoHost) { (responce, status) in
                loader.dismiss(animated: false) {
                    if(status){
                        let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                        let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                        if(apiStatus == "True"){
                            let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                            
                            if(arryData.count != 0){
                                let hostCode = (arryData.object(at: 0)as! NSDictionary).value(forKey: "CoHostCode")!
                                let dict = self.arySelectedList.object(at: tagIndex)as! NSDictionary
                                
                                let shareText = "I have been using the Nimantran app and i need your help to plan \(dict.value(forKey: "EventName")!)\nhttps://play.google.com/store/apps/details?id=com.info.nimantran\n Install it and join to \(dict.value(forKey: "EventName")!) by enterning this code:\(hostCode)\nSee you in the app!"
                                let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
                                self.present(vc, animated: true)
                            }
                            
                        }else{
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                        }
                    }else{
                        
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                    }
                }
                
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    func JoinCoHostCode(CoHostCode : String) {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .actionSheet)
            let GetCoHost = "\(URL_UpdateCoHostCode)UserId=\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)&CoHostCode=\(CoHostCode)"
            WebServiceClass.getRequestWithHeadersGET(url: GetCoHost) { (responce, status) in
                loader.dismiss(animated: false) {
                    if(status){
                        let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                        let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                        if(apiStatus == "True"){
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 2)
                            self.getEventLiastByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
                            
                        }else{
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                        }
                    }else{
                        
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                    }
                }
                
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    
}
// MARK:
// MARK:- DrawerScreenDelegate
extension DashBoardVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        
        if(strType == "Profile"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC")as!ProfileVC
           self.navigationController?.pushViewController(testController, animated: true)
        }
        
        
       else if(tag == 99){
            let alert = UIAlertController(title: alertMessage, message: alertLogout, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                print("Yay! You brought your towel!")
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "TutorialVC")as!TutorialVC
                self.navigationController?.pushViewController(testController, animated: true)
            }))
            self.present(alert, animated: true)
        }else if(tag == 0){ // My A/c
            
            
        }else if(tag == 1){ // My AddressBook
            
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddressBookVC")as!AddressBookVC
            testController.strViewComeFrom = "AddNewContactInAddressBook"
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if(tag == 2){ // AddCoHost
                       
                       AddCoHostForEvent()
                       
        }
        else if(tag == 3){ // Sent
            
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "SentVC")as!SentVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
            else if(tag == 4){ // RSVP
                    
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "RSVP_VC")as!RSVP_VC
                    self.navigationController?.pushViewController(testController, animated: true)
                }
                   
       
        else if(tag == 5){ // About App
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "WebClass") as? WebClass
            vc?.titleString = strType
            vc?.urlString = URL_aboutapp
            self.present((vc)!, animated: true) {
            }
        }else if(tag == 6){ // Term's Of Use
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "WebClass") as? WebClass
            vc?.titleString = strType
            vc?.urlString = URL_termsofuse
            self.present((vc)!, animated: true) {
            }
        }else if(tag == 7){ // Help Support
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "WebClass") as? WebClass
            vc?.titleString = strType
            vc?.urlString = URL_helpsupport
            self.present((vc)!, animated: true) {
            }
        }else if(tag == 8){ // Change Password
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChangePAsswordVC") as? ChangePAsswordVC
            self.present((vc)!, animated: true) {
            }
            
        }
    }
}
// MARK: -
// MARK: -UITableViewDelegate

extension DashBoardVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "EventCell", for: indexPath as IndexPath) as! EventCell
        if(arySelectedList.count != 0){
            let dict = arySelectedList.object(at: indexPath.row)as! NSDictionary
            cell.lbl_EventTitle.text = "\(dict.value(forKey: "EventName")!)"
            cell.lbl_EventName.text = "\(dict.value(forKey: "AboutEvent")!)"
            cell.lbl_EventAddress.text = "\(dict.value(forKey: "EventAddress")!)"
            cell.lbl_EventDate.text = "Date: \(dict.value(forKey: "EventFromDate")!)"
            
            cell.btnContact.tag = indexPath.row
            cell.btnContact.addTarget(self, action: #selector(buttonCalling), for: .touchUpInside)
            cell.btnContact.setTitle("\(dict.value(forKey: "ContactNumber")!)", for: .normal)
            cell.btnAddGuest.tag = indexPath.row
            cell.btnReminder.tag = indexPath.row
            
            
            cell.btnAddGuest.addTarget(self, action: #selector(buttonGuest), for: .touchUpInside)
            cell.btnReminder.addTarget(self, action: #selector(buttonReminder), for: .touchUpInside)
            
            
            cell.btnReminder.layer.cornerRadius = 8.0
            cell.btnAddGuest.layer.cornerRadius = 8.0
            
            cell.btnReminder.layer.borderWidth = 1.0
            cell.btnAddGuest.layer.borderWidth = 1.0
            
            cell.btnReminder.layer.borderColor = UIColor.lightGray.cgColor
            cell.btnAddGuest.layer.borderColor = UIColor.lightGray.cgColor
        
            cell.lbl_EventStatic.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            cell.lbl_EventStatic.frame = CGRect(x: 0, y: 3, width: 15, height: cell.btnAddCoHost.frame.maxY)
            
            let userID = "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)"
            let event_userID = "\(dict.value(forKey: "UserId")!)"
            
            if((userID == event_userID)){
                cell.btnAddCoHost.layer.borderColor = UIColor.lightGray.cgColor
                cell.btnAddCoHost.layer.borderWidth = 1.0
                cell.btnAddCoHost.layer.cornerRadius = 8.0
                cell.btnAddCoHost.tag = indexPath.row
                cell.btnAddCoHost.addTarget(self, action: #selector(buttonAddCoHost), for: .touchUpInside)
                cell.btnAddCoHost.setTitle("Add a Co-Host", for: .normal)
                cell.btnAddCoHost.isUserInteractionEnabled = true
                cell.btnAddCoHost.titleLabel?.textAlignment = .center
                cell.lbl_Co_host.text = ""
            }else{
                cell.btnAddCoHost.titleLabel?.textAlignment = .right
                cell.btnAddCoHost.layer.borderColor = UIColor.clear.cgColor
                cell.btnAddCoHost.layer.borderWidth = 0.0
                cell.btnAddCoHost.layer.cornerRadius = 0.0
                cell.btnAddCoHost.setTitle("", for: .normal)
                cell.btnAddCoHost.isUserInteractionEnabled = false
                cell.lbl_Co_host.text = "You are a Co-Host"
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(arySelectedList.count != 0){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "EventDetailVC") as? EventDetailVC
            vc?.dictEventData = (arySelectedList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }
    func tableView (_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //I read I need this, so I have it
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let dict = arySelectedList.object(at: indexPath.row)as! NSDictionary
        let userID = "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)"
        let event_userID = "\(dict.value(forKey: "UserId")!)"
        var dlt_Remove = true
        var dlt_RemoveOption = "Delete"
        
        
        
        (userID == event_userID) ? dlt_RemoveOption = "Delete" : (dlt_RemoveOption = "Remove")
        
        (userID == event_userID) ? dlt_Remove = true : (dlt_Remove = false)
        
        // action two
        let deleteAction = UITableViewRowAction(style: .destructive, title: dlt_RemoveOption, handler: { (action, indexPath) in
            let alert = UIAlertController(title: alertMessage, message: (userID == event_userID) ? alertEventDelete : alertEventRemove, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: dlt_RemoveOption, style: .destructive , handler:{ (UIAlertAction)in
                let dict = self.arySelectedList.object(at: indexPath.row)as! NSDictionary
                
                
                if(dlt_Remove){
                    self.getEventDelete(eventID: "\(dict.value(forKey: "EventId")!)", tagIndex: indexPath.row)
                    
                }else{
                    self.DeleteCoHostEvent(userID: userID, eventID: "\(dict.value(forKey: "EventId")!)", tagIndex: indexPath.row)
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Dismiss button")
            }))
            
            self.present(alert, animated: true, completion: {
            })
        })
        return [deleteAction]
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    @objc func buttonAddCoHost(_ sender: UIButton!) {
        let dict = self.arySelectedList.object(at: sender.tag)as! NSDictionary
        GenerateCoHost(eventID: "\(dict.value(forKey: "EventId")!)", tagIndex: sender.tag)
    }
    @objc func buttonReminder(_ sender: UIButton!) {
        print("Button tapped")
        let dict = arySelectedList.object(at: sender.tag)as! NSDictionary
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddReminderVC") as? AddReminderVC
        vc?.dictEventData = dict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func buttonGuest(_ sender: UIButton!) {
        print("Button tapped")
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddGuestVC") as? AddGuestVC
        vc?.dictEventData = (arySelectedList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func buttonCalling(_ sender: UIButton!) {
        
        if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
        }
    }
        
  
    
    
}
// MARK: - ----------------EventCell
// MARK: -
class EventCell: UITableViewCell {
    @IBOutlet weak var lbl_EventTitle: UILabel!
    @IBOutlet weak var lbl_EventName: UILabel!
    @IBOutlet weak var lbl_EventAddress: UILabel!
    @IBOutlet weak var lbl_EventDate: UILabel!
    
    @IBOutlet weak var btnAddGuest: UIButton!
    @IBOutlet weak var btnReminder: UIButton!
    @IBOutlet weak var btnAddCoHost: UIButton!
    
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var lbl_EventStatic: UILabel!
    @IBOutlet weak var lbl_Co_host: UILabel!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK: -
// MARK: - UISearchBarDelegate


extension DashBoardVC: UISearchBarDelegate  {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
        self.view.endEditing(true)
    }
    
    
    func searchAutocomplete(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "EventName contains[c] %@ OR EventAddress contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
        }
        else{
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = self.aryList.mutableCopy() as! NSMutableArray
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(arySelectedList.count == 0){
            self.strErrorMessage = alertDataNotFound
        }
        
        DispatchQueue.main.async {
            self.numberOfSections = 1
            self.numberOfRows = self.arySelectedList.count
            self.tableView.reloadData()
        }
    }
}
//MARK
//MARK:Get FCM Token

extension DashBoardVC : UNUserNotificationCenterDelegate{
    func getTokenFCM() {
        
        //MARK:Get Token
        let center1 = UNUserNotificationCenter.current()
        center1.delegate = self
        center1.getNotificationSettings(completionHandler: { settings in
            switch settings.authorizationStatus {
            case .authorized, .provisional:
                print("authorized")
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    
                    InstanceID.instanceID().instanceID { (result, error) in
                        if let error = error {
                            print("Error fetching remote instange ID: \(error)")
                        } else if let result = result {
                            nsud.setValue("\(result.token)", forKey: "NIM_FCM_Token")
                            nsud.synchronize()
                            
                            print("Remote instance ID token: \(result.token)")
                        }
                    }
                    
                }
            case .denied:
                break
            case .notDetermined:
                break
            @unknown default:
                break
            }
        })
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                nsud.setValue("\(result.token)", forKey: "NIM_FCM_Token")
                nsud.synchronize()
                
                print("Remote instance ID token: \(result.token)")
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "NIM_FCM_Token")
        nsud.synchronize()
    }
    
}
