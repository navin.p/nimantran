//
//  SignInVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class SignInVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var btnLogin: TransitionButton!
    @IBOutlet weak var btnKeepMelogin: UIButton!
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnKeepMelogin.setImage(UIImage(named: "uncheck"), for: .normal)
        txtMobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    override func viewWillLayoutSubviews() {
        for item in self.btnLogin.layer.sublayers! {
            if item is CAGradientLayer {
                item.removeFromSuperlayer()
            }
        }
        btnLogin.layer.insertSublayer(getgradientLayer(bounds: btnLogin.bounds), at: 0)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {

                    let text = textField.text ?? ""

        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))

                    textField.text = trimmedText
               if(txtMobileNumber == textField){
                 textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
             }
        }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnLogin(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(validationForSignIN()){
            self.LoginAPI(mobileNumber: txtMobileNumber.text!, Password: txtPassword.text!)
        }
        
        
    }
    @IBAction func actionOnNewAccount(_ sender: TransitionButton) {
        self.view.endEditing(true)
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SignUpVC") as? SignUpVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func actionOnKeepMelogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if(btnKeepMelogin.currentImage == UIImage(named: "uncheck")){
            btnKeepMelogin.setImage(UIImage(named: "check"), for: .normal)
        }else{
            btnKeepMelogin.setImage(UIImage(named: "uncheck"), for: .normal)
        }
    }
    
}
// MARK:
// MARK:- Validation

extension SignInVC {
    func validationForSignIN() -> Bool {
        if(txtMobileNumber.text?.count == 0){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumber)
            
            return false
        }else if(txtMobileNumber.text!.count < 10){
            
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumberValid)
            return false
            
        }else if(txtPassword.text!.count == 0){
            addErrorMessage(textView: txtPassword, message: alert_Password)
            return false
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension SignInVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK: -
// MARK: - API Calling


extension SignInVC  {
    
    func LoginAPI(mobileNumber : String , Password : String) {
        if(isInternetAvailable()){
            self.btnLogin.startAnimation()
            let urlLogin = "\(URL_GetLogin)MobileNumber=\(mobileNumber)&Password=\(Password)"
            WebServiceClass.getRequestWithHeadersGET(url: urlLogin) { (responce, status) in
                if(status){
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if(apiStatus == "True"){
                        self.btnLogin.stopAnimation(animationStyle: .expand, revertAfterDelay: 00) {
                            let DataLogin = (((responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary)
                            let post_paramsValue = DataLogin.nullKeyRemoval()
                            nsud.set(post_paramsValue, forKey: "Nim_LoginData")
                            if(self.btnKeepMelogin.currentImage == UIImage(named: "uncheck")){
                                nsud.set(false, forKey: "Nim_KeepMeLogin")
                            }else{
                                nsud.set(true, forKey: "Nim_KeepMeLogin")
                            }
                            nsud.synchronize()
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                                self.navigationController?.pushViewController(vc!, animated: false)
                CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: "LogIn successfully.", dismissDelay: 3, completion: {})
                   
                        
                }
                    }else{
                        self.btnLogin.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }
                }else{
                    self.btnLogin.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                         CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
        }
    }
}
