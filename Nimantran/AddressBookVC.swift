//
//  AddressBookVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/17/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import Alamofire
import CRNotifications
import LoadingPlaceholderView
import TransitionButton


class AddressBookVC: UIViewController {
    
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnaddRecipient: TransitionButton!
    @IBOutlet weak var btnaddRecipient_Height: NSLayoutConstraint!
    @IBOutlet weak var viewHold: UIView!
    @IBOutlet weak var txtSearch: UISearchBar!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    var aryAll = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var aryAddRecipientSelectedList = NSMutableArray()
    
    var strErrorMessage  = ""
    var strViewComeFrom  = ""
    var dictEventData  = NSDictionary()
    
    
    private var numberOfSections = 0
    private var numberOfRows = 0
    private var loadingPlaceholderView = LoadingPlaceholderView(){
        didSet{
            do {
                loadingPlaceholderView.gradientColor = .white
                loadingPlaceholderView.backgroundColor = .white
            }
        }
    }
    var refreshControl = UIRefreshControl()
    
    private var cellsIdentifiers = [
        "AddressBookCell",
        "AddressBookCell","AddressBookCell",
        "AddressBookCell","AddressBookCell",
        "AddressBookCell"
    ]
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictEventData = (dictEventData.mutableCopy()as! NSMutableDictionary).removeNullFromDict()
        txtSearch.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Get Event list...")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        if(isInternetAvailable()){
            loadingPlaceholderView.cover(viewHold, animated: true)
            getAddressByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
        }else{
            self.aryAll = NSMutableArray()
            self.strErrorMessage = alertInternet
            self.tableView.reloadData()
        }
        // tableView.addSubview(refreshControl)
        let nc = NotificationCenter.default
        
        nc.addObserver(self, selector: #selector(refreshData), name: Notification.Name("NIM_AddNewAddress"), object: nil)
        drawButtonOnScroll(indexColor: 0)
        
        strViewComeFrom == "AddNewContactInAddressBook" ? btnaddRecipient_Height.constant = 0.0 : (btnaddRecipient_Height.constant = 45.0)
        strViewComeFrom == "AddNewContactInAddressBook" ? lblTitle.text = "My Address Book"  : (lblTitle.text = "Add Recipient")
        
    }
    
    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: 100 * 6, height: 40)
        for item in self.btnaddRecipient.layer.sublayers! {
            if item is CAGradientLayer {
                item.removeFromSuperlayer()
            }
        }
        btnaddRecipient.layer.insertSublayer(getgradientLayer(bounds: btnaddRecipient.bounds), at: 0)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func drawButtonOnScroll(indexColor : Int) {
        let aryBtn = NSMutableArray()
        aryBtn.add("All")
        aryBtn.add("Home")
        aryBtn.add("Friend")
        aryBtn.add("Work")
        aryBtn.add("Personal")
        aryBtn.add("Other")
        
        for index in 0..<aryBtn.count {
            let frame1 = CGRect(x: 10 + (index * 100), y: 8 , width: 90, height: 34 )
            let button = UIButton(frame: frame1)
            button.tag = index
            button.setTitle((aryBtn.object(at: index) as! String), for: .normal)
            button.addTarget(self, action: #selector(buttonScroll(_:)), for: .touchUpInside)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            if(indexColor == index){
                button.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
                button.setTitleColor(UIColor.white, for: .normal)
            }else{
                button.backgroundColor = UIColor.white
                button.setTitleColor(UIColor.black, for: .normal)
            }
            button.layer.cornerRadius = 17
            self.scroll.addSubview(button)
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        txtSearch.text = ""
        getAddressByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
        
        
    }
    @objc func refreshData() {
        txtSearch.text = ""
        getAddressByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
    }
    
    @objc func buttonScroll(_ sender: UIButton!) {
        drawButtonOnScroll(indexColor: sender.tag)
        if(sender.tag == 0){
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = aryAll
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
        }else{
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = fliterData(arryMain: self.aryAll, type: (sender.titleLabel?.text!)!)
        }
        if(self.arySelectedList.count == 0){
            self.strErrorMessage =  alertDataNotFound
        }else{
            self.strErrorMessage =  ""
        }
        DispatchQueue.main.async {
            self.numberOfSections = 1
            self.numberOfRows = self.arySelectedList.count
            self.tableView.reloadData()
        }
    }
    
    // MARK:
    // MARK:- IBAction

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnAdd(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddNewAddressVC") as? AddNewAddressVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func actionOnGuest(_ sender: UIButton) {
        self.view.endEditing(true)
        print(aryAddRecipientSelectedList)
        let arySend = NSMutableArray()
        for item in aryAddRecipientSelectedList {
            let dict = NSMutableDictionary()
            dict.setValue("0", forKey: "EventNimantranId")
            dict.setValue("\(self.dictEventData.value(forKey: "EventId")!)", forKey: "EventId")
            dict.setValue("\((item as AnyObject).value(forKey: "UserId")!)", forKey: "UserId")
            dict.setValue("True", forKey: "IsAttend")
            dict.setValue("0", forKey: "NoOfPerson")
            dict.setValue("\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", forKey: "InviteByUserId")

            dict.setValue("\(self.dictEventData.value(forKey: "IsGenerateQRCode")!)", forKey: "IsGenerateQRCode")

            arySend.add(dict)
        }
        
        if(aryAddRecipientSelectedList.count != 0){
            APIAddNewGuest(Jsondict: arySend)
        }else{
            // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
        }
    }
    
    @IBAction func actionOnImportExport(_ sender: UIButton) {
           self.view.endEditing(true)
         let vc = mainStoryboard.instantiateViewController(withIdentifier: "ImportExportVC") as? ImportExportVC
         self.navigationController?.pushViewController(vc!, animated: true)
       }
    
    
    func fliterData(arryMain : NSMutableArray , type : String) -> NSMutableArray {
        let resultPredicate = NSPredicate(format: "AddressType contains[c] %@ OR AddressType contains[c] %@", argumentArray: [type, type])
        let arrayfilter = (self.aryAll ).filtered(using: resultPredicate)
        let nsMutableArray = NSMutableArray(array: arrayfilter)
        self.arySelectedList = NSMutableArray()
        self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
        return self.arySelectedList
    }
    
    
    
    // MARK:
    // MARK:- API Calling
    func getAddressByUserID(userID : String , tag : Int) {
        // self.segment.selectedSegmentIndex = 0
        let dict = NSMutableDictionary()
        dict.setValue("0", forKey: "AddressBookId")
        dict.setValue("\(userID)", forKey: "HostUserId")
        dict.setValue("0", forKey: "EventId")
        
        //  var jsoneString = getJson(from: dict)
        
        WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GetAddressBook) { (responce, status) in
            
            if(status){
                let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                if(apiStatus == "True"){
                    let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                    print("----------\(arryData)")
                    self.aryAll = NSMutableArray()
                    self.aryAll = arryData.mutableCopy()as! NSMutableArray
                    self.arySelectedList = NSMutableArray()
                    self.arySelectedList = arryData.mutableCopy()as! NSMutableArray
                    self.strErrorMessage = ""
                }else{
                    self.arySelectedList = NSMutableArray()
                    self.aryAll = NSMutableArray()
                    self.strErrorMessage = apiError
                }
            }else{
                self.arySelectedList = NSMutableArray()
                self.aryAll = NSMutableArray()
                self.strErrorMessage = alertSomeError
            }
            
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.loadingPlaceholderView.uncover(animated: true)
                self.numberOfSections = 1
                self.numberOfRows = self.arySelectedList.count
                self.tableView.reloadData()
                self.drawButtonOnScroll(indexColor: 0)
            }
        }
        
        
    }
    
    func AddressDelete(AddressBookId : String ,tagIndex : Int) {
        
        if(isInternetAvailable()){
            
            let loader = loader_Show(controller: self, strMessage: "Deleting address...", title: "Please wait...", style: .actionSheet)

            let urlForgotPass = "\(URL_DeleteAddressBook)AddressBookId=\(AddressBookId)"
            WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                loader.dismiss(animated: true, completion: nil)
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        CRNotifications.showNotification(type: CRNotifications.success, title: alertMsg, message: apiError, dismissDelay: 2)
                        self.aryAll.remove(self.arySelectedList.object(at: tagIndex))
                        self.arySelectedList.removeObject(at: tagIndex)
                        DispatchQueue.main.async {
                            self.numberOfSections = 1
                            self.numberOfRows = self.arySelectedList.count
                            self.tableView.reloadData()
                        }
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                    }
                }else{
                    
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    func APIAddNewGuest(Jsondict : NSArray) {
        if(isInternetAvailable()){
            self.btnaddRecipient.startAnimation()
            WebServiceClass.getRequestWithHeadersWithArray(JsonArray: Jsondict, url: AddUpdateEventAddressBookNimantran) { (responce, status) in
                
                self.btnaddRecipient.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
                    if(status){
                        
                        let resultData = (responce.value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary
                        
                        let apiStatus = "\(resultData.value(forKey: "Result")!)"
                        let apiError = "\(resultData.value(forKey: "Success")!)"
                        if(apiStatus == "True"){
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 2)
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name("NIM_AddGuest"), object: nil)
                            self.navigationController?.popViewController(animated: true)
                            
                        }else{
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
        }
        else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
            
        }
    }
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddressBookVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "AddressBookCell", for: indexPath as IndexPath) as! AddressBookCell
        
        if(arySelectedList.count != 0){
            
            let dict = arySelectedList.object(at: indexPath.row)as! NSDictionary
            cell.lbl_Name.text = "\(dict.value(forKey: "Name")!)"
            cell.lbl_Type.text = "\(dict.value(forKey: "AddressType")!)"
            
            cell.btnEmail.tag = indexPath.row
            cell.btnEmail.addTarget(self, action: #selector(buttonMail), for: .touchUpInside)
            cell.btnEmail.setTitle("\(dict.value(forKey: "EmailId")!)", for: .normal)
            
            cell.btnNumber.tag = indexPath.row
            cell.btnNumber.addTarget(self, action: #selector(buttonCalling), for: .touchUpInside)
            cell.btnNumber.setTitle("\(dict.value(forKey: "MobileNumber")!)", for: .normal)
            cell.lbl_EventStatic.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            cell.lbl_EventStatic.frame = CGRect(x: 0, y: 3, width: 15, height: cell.btnNumber.frame.maxY)
            // For Add Guest
            strViewComeFrom == "AddNewContactInAddressBook" ? cell.btnCheck.isHidden = true : (cell.btnCheck.isHidden = false )
            if(aryAddRecipientSelectedList.contains(dict)){
                cell.btnCheck.setImage(UIImage(named: "check"), for: .normal)
            }else{
                cell.btnCheck.setImage(UIImage(named: "uncheck"), for: .normal)
            }
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(buttonAddRecipient(_:)), for: .touchUpInside)
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(arySelectedList.count != 0){
            
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 20))
        if #available(iOS 13.0, *) {
            customView.backgroundColor = UIColor.systemBackground
        } else {
            customView.backgroundColor = UIColor.white
        }
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        button.setTitle("Total Address Book: \(arySelectedList.count)", for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        // action one
        let editAction = UITableViewRowAction(style: .normal, title: "Edit", handler: { (action, indexPath) in
            print("Edit tapped")
            let dict = self.arySelectedList.object(at: indexPath.row)as! NSDictionary
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddNewAddressVC") as? AddNewAddressVC
            vc!.strViewComeFrom = "UPDATE"
            vc!.dictData = dict
            self.navigationController?.pushViewController(vc!, animated: true)
            
        })
        // editAction.backgroundColor = UIColor.blue
        
        // action two
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: { (action, indexPath) in
            let alert = UIAlertController(title: alertMessage, message: alertAddressDelete, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                let dict = self.arySelectedList.object(at: indexPath.row)as! NSDictionary
                self.AddressDelete(AddressBookId: "\(dict.value(forKey: "AddressBookId")!)", tagIndex: indexPath.row)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Dismiss button")
            }))
            
            self.present(alert, animated: true, completion: {
            })
        })
        // deleteAction.backgroundColor = UIColor.red
        
        return [deleteAction,editAction]
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    @objc func buttonMail(_ sender: UIButton!) {
        print("Button tapped")
    }
    @objc func buttonAddRecipient(_ sender: UIButton!) {
        let dict = arySelectedList.object(at: sender.tag)as! NSDictionary
        if(aryAddRecipientSelectedList.contains(dict)){
            self.aryAddRecipientSelectedList.remove(dict)
        }else{
            self.aryAddRecipientSelectedList.add(dict)
        }
        self.tableView.reloadData()
        print("Button tapped")
    }
    
    @objc func buttonCalling(_ sender: UIButton!) {
        
        if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
        }
        
        
    }
}
// MARK: - ----------------ProfileCell
// MARK: -
class AddressBookCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Type: UILabel!
    
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnNumber: UIButton!
    @IBOutlet weak var lbl_EventStatic: UILabel!
    
    @IBOutlet weak var btnCheck: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK: -
// MARK: - UISearchBarDelegate


extension AddressBookVC: UISearchBarDelegate  {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
        self.view.endEditing(true)
    }
    
    
    
    func searchAutocomplete(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR MobileNumber contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryAll ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
        }
        else{
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = self.aryAll.mutableCopy() as! NSMutableArray
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(arySelectedList.count == 0){
            self.strErrorMessage = alertDataNotFound
            
        }
        
        DispatchQueue.main.async {
            self.numberOfSections = 1
            self.numberOfRows = self.arySelectedList.count
            self.tableView.reloadData()
        }
        
        
    }
}
