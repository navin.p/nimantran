//
//  AddReminderVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/19/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import TransitionButton

class AddReminderVC: UIViewController {
   
    
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var txtDate: ACFloatingTextfield!
    @IBOutlet weak var txtTime: ACFloatingTextfield!
    @IBOutlet weak var viewHold: UIView!
    @IBOutlet weak var btnAddReminder: TransitionButton!

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    // MARK:
     // MARK:- var
     var dictEventData = NSDictionary()
    var aryList = NSMutableArray()
      var strErrorMessage  = ""
       private var numberOfSections = 0
       private var numberOfRows = 0
       private var loadingPlaceholderView = LoadingPlaceholderView(){
           didSet{
               do {
                   loadingPlaceholderView.gradientColor = .white
                   loadingPlaceholderView.backgroundColor = .white
               }
           }
       }
       var refreshControl = UIRefreshControl()
       
       private var cellsIdentifiers = [
           "ReminderCell",
           "ReminderCell","ReminderCell",
           "ReminderCell","ReminderCell",
           "ReminderCell"
       ]
       
    // MARK:
    // MARK:- lifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

              refreshControl.attributedTitle = NSAttributedString(string: "Get reminder list...")
              refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
              if(isInternetAvailable()){
                  loadingPlaceholderView.cover(viewHold, animated: true)
                  getReminderLiastByUserID(eventID: "\(dictEventData.value(forKey: "EventId")!)", tag: 0)
              }else{
                  self.aryList = NSMutableArray()
                  self.strErrorMessage = alertInternet
                  self.tableView.reloadData()
              }
              tableView.addSubview(refreshControl)
    }
    @objc func refresh(sender:AnyObject) {
        getReminderLiastByUserID(eventID: "\(dictEventData.value(forKey: "EventId")!)", tag: 0)
          
      }
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func actionOnAddNewReminder(_ sender: UIButton) {
        if(validationForAddReminder()){
            
            
            let dict = NSMutableDictionary()
            //02/21/2020 06:21:32.333 AM

                   dict.setValue("0", forKey: "EventReminderId")
                   dict.setValue("\(self.dictEventData.value(forKey: "EventId")!)", forKey: "EventId")
                   dict.setValue("\(txtDate.text!) \(txtTime.text!)", forKey: "ReminderDate")
            APIAddNewReminder(Jsondict: dict)
        }

    
    }
    
    @IBAction func actionOnDate(_ sender: UIButton) {
        self.view.endEditing(true)
         
        
          let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
           sender.tag == 1 ? vc.strMode = "Date" : (vc.strMode = "Time")
           vc.strTag = sender.tag
           vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
           vc.modalTransitionStyle = .coverVertical
           vc.delegateDateTime = self
           self.present(vc, animated: true, completion: {})
       }
    
       
    // MARK:
    // MARK:- Call API
    
      func getReminderLiastByUserID(eventID : String , tag : Int) {
          
          if(isInternetAvailable()){
              let urlForgotPass = "\(URL_GetEventReminder)EventId=\(eventID)"
              WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                self.loadingPlaceholderView.uncover(animated: true)
                self.refreshControl.endRefreshing()
                  if(status){
                      let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                      let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      if(apiStatus == "True"){
                         
                        let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                                           self.aryList = NSMutableArray()
                                           self.aryList = arryData.mutableCopy()as! NSMutableArray
                                           self.strErrorMessage = ""
                        DispatchQueue.main.async {
                            self.numberOfSections = 1
                            self.numberOfRows = self.aryList.count
                            self.tableView.reloadData()
                        }
                      }else{
                        self.aryList = NSMutableArray()
                        self.strErrorMessage = apiError
                        self.numberOfSections = 1
                        self.numberOfRows = self.aryList.count
                        self.tableView.reloadData()
                         // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                      }
                  }else{
                    self.aryList = NSMutableArray()
                    self.strErrorMessage = alertSomeError
                    self.numberOfSections = 1
                    self.numberOfRows = self.aryList.count
                    self.tableView.reloadData()
                     // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                  }
              }
          }else{
            self.aryList = NSMutableArray()
            self.strErrorMessage = alertSomeError
            self.numberOfSections = 1
            self.numberOfRows = self.aryList.count
            self.tableView.reloadData()
             // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
          }
      }
    func APIAddNewReminder(Jsondict : NSDictionary) {
          if(isInternetAvailable()){
              self.btnAddReminder.startAnimation()
          WebServiceClass.getRequestWithHeaders(dictJson: Jsondict, url: URL_AddUpdateEventReminder) { (responce, status) in
                  
            self.btnAddReminder.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {}
            self.txtDate.text = ""
            self.txtTime.text = ""
            if(status){
                      let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                      let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      if(apiStatus == "True"){
                           CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 2)
                        self.getReminderLiastByUserID(eventID: "\(self.dictEventData.value(forKey: "EventId")!)", tag: 0)

                      }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)

                      }
                  }else{
                     CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)

                  }
              
              }
              
              
          }
          else{
              CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
              
          }
      }
}
//MARK:-
//MARK:- ---------Date Time Selection Delegate
extension AddReminderVC : DateTimeDelegate{
    func getDataFromDelegate(time: Date, tag: Int) {
        let dateFormatter = DateFormatter()
        if(tag == 1){ // For Date
                   dateFormatter.dateFormat = "MM/dd/yyyy"
                   self.txtDate.text = "\(dateFormatter.string(from: time))"

               }else if (tag == 2){ // for time
                   dateFormatter.dateFormat = "hh:mm a"
                   self.txtTime.text = "\(dateFormatter.string(from: time))"
               }
    }
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddReminderVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "ReminderCell", for: indexPath as IndexPath) as! ReminderCell
        if(aryList.count != 0){
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            let strDAteTime = getDateTime(FormateDate: "MM/dd/yyyy", FormateTime: "hh:mm a", strdate: "\(dict.value(forKey: "ReminderDate")!)")
            
            
            
            
            if(strDAteTime.split(separator: ",").count == 2){
                cell.lbl_EventDate.text = "Date: \(strDAteTime.split(separator: ",")[0])"
                cell.lbl_EventTime.text = "Time: \(strDAteTime.split(separator: ",")[1])"

            }
            cell.lbl_EventStatic.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            cell.lbl_EventStatic.frame = CGRect(x: 0, y: 3, width: 15, height: 60)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
         
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }

    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }

 
    @objc func buttonCalling(_ sender: UIButton!) {
        
        if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
        }
        
        
    }
}
// MARK: -
// MARK: -ReminderCell
class ReminderCell: UITableViewCell {
    @IBOutlet weak var lbl_EventTime: UILabel!
   @IBOutlet weak var lbl_EventDate: UILabel!

    @IBOutlet weak var lbl_EventStatic: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK:
// MARK:- Validation

extension AddReminderVC {
    func validationForAddReminder() -> Bool {
     if(txtDate.text?.count == 0){
                                             addErrorMessage(textView: txtDate, message: alert_required)
                                             
                                             return false
                                         }
            else if(txtTime.text?.count == 0){
                                                      addErrorMessage(textView: txtTime, message: alert_required)
                                                      
                                                      return false
                                                  }
     
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
