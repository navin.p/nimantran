//
//  AddGuestVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/20/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import TransitionButton

class AddGuestVC: UIViewController {
    // MARK:
       // MARK:- IBOutlet
    
       @IBOutlet weak var viewHold: UIView!
       @IBOutlet weak var btnNext: TransitionButton!
       @IBOutlet weak var btnAdd: UIButton!

       @IBOutlet private weak var tableView: UITableView! {
           didSet {
               tableView.coverableCellsIdentifiers = cellsIdentifiers
               tableView.tableFooterView = UIView()
               tableView.estimatedRowHeight = 200
           }
       }
       // MARK:
        // MARK:- var
        var dictEventData = NSMutableDictionary()
       var aryList = NSMutableArray()
         var strErrorMessage  = ""
          private var numberOfSections = 0
          private var numberOfRows = 0
          private var loadingPlaceholderView = LoadingPlaceholderView(){
              didSet{
                  do {
                      loadingPlaceholderView.gradientColor = .white
                      loadingPlaceholderView.backgroundColor = .white
                  }
              }
          }
          var refreshControl = UIRefreshControl()
          
          private var cellsIdentifiers = [
              "AddressBookCell",
              "AddressBookCell","AddressBookCell",
              "AddressBookCell","AddressBookCell",
              "AddressBookCell"
          ]
          // MARK:
           // MARK:- lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAdd.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
        btnAdd.layer.cornerRadius = 12.0
           refreshControl.attributedTitle = NSAttributedString(string: "Get reminder list...")
                  refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
                  if(isInternetAvailable()){
                      loadingPlaceholderView.cover(viewHold, animated: true)
                    
                    let strLoginUserID = "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)"
                    
                    let strEventUserID =  "\(dictEventData.value(forKey: "EventId")!)"
                    
                    let dict = NSMutableDictionary()
                              dict.setValue("\(dictEventData.value(forKey: "EventId")!)", forKey: "EventId")
                    if(strLoginUserID == strEventUserID){
                        dict.setValue("0", forKey: "InviteByUserId")

                    }else{
                        dict.setValue("\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", forKey: "InviteByUserId")

                    }
                              
                    
                    getGuestListByUserID(dict: dict, tag: 0)
                  }else{
                      self.aryList = NSMutableArray()
                      self.strErrorMessage = alertInternet
                      self.tableView.reloadData()
                  }
                  tableView.addSubview(refreshControl)
        
        let nc = NotificationCenter.default
         nc.addObserver(self, selector: #selector(refresh), name: Notification.Name("NIM_AddGuest"), object: nil)
    }
    
    
    @objc func refresh(sender:AnyObject) {
        let strLoginUserID = "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)"
        let strEventUserID =  "\(dictEventData.value(forKey: "EventId")!)"
        let dict = NSMutableDictionary()
        dict.setValue("\(dictEventData.value(forKey: "EventId")!)", forKey: "EventId")
        if(strLoginUserID == strEventUserID){
            dict.setValue("0", forKey: "InviteByUserId")
            
        }else{
            dict.setValue("\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", forKey: "InviteByUserId")
        }
        getGuestListByUserID(dict: dict, tag: 0)
        
    }
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func actionOnNext(_ sender: UIButton) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "EventDetailVC") as? EventDetailVC
        vc?.dictEventData = self.dictEventData
        vc?.aryRecipientList = self.aryList
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func actionOnAdd(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddressBookVC")as!AddressBookVC
        testController.strViewComeFrom = ""
        testController.dictEventData = self.dictEventData
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
     
       
    // MARK:
    // MARK:- Call API
    
      func getGuestListByUserID(dict : NSDictionary , tag : Int) {
          if(isInternetAvailable()){
               WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GetEventAddressBookNimantran) { (responce, status) in
                self.loadingPlaceholderView.uncover(animated: true)
                self.refreshControl.endRefreshing()
                                      if(status){
                      let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                      let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      if(apiStatus == "True"){
                         
                        let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                                           self.aryList = NSMutableArray()
                                           self.aryList = arryData.mutableCopy()as! NSMutableArray
                                           self.strErrorMessage = ""
                        DispatchQueue.main.async {
                            self.numberOfSections = 1
                            self.numberOfRows = self.aryList.count
                            self.tableView.reloadData()
                        }
                      }else{
                        self.aryList = NSMutableArray()
                        self.strErrorMessage = apiError
                        self.numberOfSections = 1
                        self.numberOfRows = self.aryList.count
                        self.tableView.reloadData()
                         // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                      }
                  }else{
                    self.aryList = NSMutableArray()
                    self.strErrorMessage = alertSomeError
                    self.numberOfSections = 1
                    self.numberOfRows = self.aryList.count
                    self.tableView.reloadData()
                     // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                  }
              }
          }else{
            self.aryList = NSMutableArray()
            self.strErrorMessage = alertSomeError
            self.numberOfSections = 1
            self.numberOfRows = self.aryList.count
            self.tableView.reloadData()
             // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
          }
      }
    
    
    
    
    func GuestDelete(guestEventID : String ,tagIndex : Int) {
        
        if(isInternetAvailable()){
            
            let loader = loader_Show(controller: self, strMessage: "Deleting address...", title: "Please wait...", style: .actionSheet)

            let urlForgotPass = "\(URL_DeleteEventAddressBookNimantran)EventNimantranId=\(guestEventID)"
            WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                loader.dismiss(animated: true, completion: nil)
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        CRNotifications.showNotification(type: CRNotifications.success, title:alertMsg , message: apiError, dismissDelay: 2)
                        self.aryList.removeObject(at: tagIndex)
                        DispatchQueue.main.async {
                            self.numberOfSections = 1
                            self.numberOfRows = self.aryList.count
                            self.tableView.reloadData()
                        }
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                    }
                }else{
                    
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddGuestVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "AddressBookCell", for: indexPath as IndexPath) as! AddressBookCell
       if(aryList.count != 0){
                    
                      let dict = aryList.object(at: indexPath.row)as! NSDictionary
                            cell.lbl_Name.text = "\(dict.value(forKey: "Name")!)"
                            cell.lbl_Type.text = "\(dict.value(forKey: "AddressType")!)"
                        
                        //    cell.btnEmail.tag = indexPath.row
                          //  cell.btnEmail.addTarget(self, action: #selector(buttonMail), for: .touchUpInside)
                           // cell.btnEmail.setTitle("\(dict.value(forKey: "EmailId")!)", for: .normal)
                            
                            cell.btnNumber.tag = indexPath.row
                            cell.btnNumber.addTarget(self, action: #selector(buttonCalling), for: .touchUpInside)
                            cell.btnNumber.setTitle("\(dict.value(forKey: "MobileNumber")!)", for: .normal)
                            cell.lbl_EventStatic.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
                             cell.lbl_EventStatic.frame = CGRect(x: 0, y: 3, width: 15, height: cell.btnNumber.frame.maxY)
                          

                  }
                  
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
         
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }
    func tableView (_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
          //I read I need this, so I have it
          return true
      }
      func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

             // action two
             let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: { (action, indexPath) in
                      let alert = UIAlertController(title: alertMessage, message: alertDelete, preferredStyle: .actionSheet)
                       alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                           let dict = self.aryList.object(at: indexPath.row)as! NSDictionary
                        
                        self.GuestDelete(guestEventID: "\(dict.value(forKey: "EventNimantranId")!)", tagIndex: indexPath.row)
                        
                       }))
                       
                       alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                           print("User click Dismiss button")
                       }))
                       
                       self.present(alert, animated: true, completion: {
                       })
             })

             return [deleteAction]
         }
    @objc func buttonAction(_ sender: UIButton!) {
             print("Button tapped")
         }
       @objc func buttonMail(_ sender: UIButton!) {
           print("Button tapped")
       }
    

       @objc func buttonCalling(_ sender: UIButton!) {
           
           if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
               CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
           }
           
           
       }
}
