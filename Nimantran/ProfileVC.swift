//
//  ProfileVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/25/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    // MARK:
       // MARK:- IBOutlet
       @IBOutlet weak var lblEmail: UILabel!
       @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!

    @IBOutlet weak var imgView: UIImageView!
    // MARK:
         // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

              lblName.text =  "\(getLogInData(key: "Nim_LoginData").value(forKey: "Name")!)"
         lblMobile.text =  "\(getLogInData(key: "Nim_LoginData").value(forKey: "MobileNumber")!)"
         lblEmail.text =  "\(getLogInData(key: "Nim_LoginData").value(forKey: "EmailId")!)"
        imgView.layer.cornerRadius = imgView.frame.height/2

        let urlImage = "\(URL_profileimageDownLoad)\(getLogInData(key: "Nim_LoginData").value(forKey: "ProfileImage")!)"
                       imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                           //   print(url ?? 0)
                       }, usingActivityIndicatorStyle: .gray)
    }
    
    
    
  // MARK:
  // MARK:- IBAction
  
  @IBAction func actionOnBack(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
  }
}
