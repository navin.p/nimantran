//
//  ChangePAsswordVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/12/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class ChangePAsswordVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var txtOldPass: ACFloatingTextfield!
    @IBOutlet weak var txtNewPass: ACFloatingTextfield!
    @IBOutlet weak var txtC_NewPass: ACFloatingTextfield!
    @IBOutlet weak var btnSubmit: TransitionButton!
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillLayoutSubviews() {
        for item in self.btnSubmit.layer.sublayers! {
            if item is CAGradientLayer {
                item.removeFromSuperlayer()
            }
        }
        btnSubmit.layer.insertSublayer(getgradientLayer(bounds: btnSubmit.bounds), at: 0)
    }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: TransitionButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func actionOnSubmit(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(validationForChangepass()){
            
            
        }
        
    }
    
}
// MARK:
// MARK:- Validation

extension ChangePAsswordVC {
    func validationForChangepass() -> Bool {
        if(txtOldPass.text!.count == 0){
            addErrorMessage(textView: txtOldPass, message: alert_OldPassword)
            return false
        }
        else if(txtNewPass.text!.count == 0){
            addErrorMessage(textView: txtNewPass, message: alert_NewPassword)
            return false
        }
        else if(txtC_NewPass.text!.count == 0){
            addErrorMessage(textView: txtC_NewPass, message: alert_CPassword)
            return false
        }
            
        else if(txtC_NewPass.text! != txtNewPass.text!){
            addErrorMessage(textView: txtC_NewPass, message: alert_Password_CPassword)
            return false
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension ChangePAsswordVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK: -
// MARK: - API Calling


extension ChangePAsswordVC  {
    
    func ChangePasswordAPI() {
        if(isInternetAvailable()){
            self.btnSubmit.startAnimation()
            let urlChangePassword = "\(URL_ChangePassword)UserId=\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)&OldPassword=\(txtOldPass.text!)&NewPassword=\(txtNewPass.text!)"
            WebServiceClass.getRequestWithHeadersGET(url: urlChangePassword) { (responce, status) in
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        
                        self.btnSubmit.stopAnimation(animationStyle: .normal, revertAfterDelay: 05) {
                            
                            
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else{
                        self.btnSubmit.stopAnimation(animationStyle: .shake, revertAfterDelay: 05) {
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }
                }else{
                    self.btnSubmit.stopAnimation(animationStyle: .shake, revertAfterDelay: 05) {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
        }
    }
    
}
