//
//  AddNewAddressVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/18/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class AddNewAddressVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgFriend: UIImageView!
    @IBOutlet weak var imgWork: UIImageView!
    @IBOutlet weak var imgPersonal: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var img_H: NSLayoutConstraint!
    @IBOutlet weak var img_W: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var txtFullName: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtLandMark: ACFloatingTextfield!
    @IBOutlet weak var btnSave: TransitionButton!
    
    
    
    var strType = ""
    var strViewComeFrom = ""
    var dictData = NSDictionary()
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        imgHome.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
        strType = "Home"
        
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        if(strViewComeFrom == "UPDATE"){
            setDataOnViewForUpdate()
        }
        
    }
    override func viewWillLayoutSubviews() {
        let w = self.view.frame.width / 5 - 10
        img_H.constant = w
        img_W.constant = w
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let text = textField.text ?? ""
        
        let trimmedText = text.trimmingCharacters(in: .whitespaces)
        
        textField.text = trimmedText
        if(txtMobile == textField){
            textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
            textField.text = textField.text?.replacingOccurrences(of: " ", with: "")
            
        }
    }
    func setDataOnViewForUpdate() {
        btnSave.setTitle("Edit Address", for: .normal)
        let tempdictData =  (dictData.mutableCopy()as! NSMutableDictionary).removeNullFromDict()
        
        txtMobile.text = "\(tempdictData.value(forKey: "MobileNumber")!)"
        txtFullName.text = "\(tempdictData.value(forKey: "Name")!)"
        txtEmail.text = "\(tempdictData.value(forKey: "EmailId")!)"
        txtLandMark.text = "\(tempdictData.value(forKey: "Address")!)"
        let strType = "\(tempdictData.value(forKey: "AddressType")!)"
        imgHome.backgroundColor = UIColor.clear
        imgFriend.backgroundColor = UIColor.clear
        imgWork.backgroundColor = UIColor.clear
        imgPersonal.backgroundColor = UIColor.clear
        imgOther.backgroundColor = UIColor.clear
        if(strType == "Home"){
            imgHome.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
        }else if(strType == "Friend"){
            imgFriend.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
        }else if(strType == "Work"){
            imgWork.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            
        }else if(strType == "Personal"){
            imgPersonal.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            
        }else if(strType == "Others"){
            imgOther.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            
        }
    }
    
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnCategory(_ sender: UIButton) {
        self.view.endEditing(true)
        imgHome.backgroundColor = UIColor.clear
        imgFriend.backgroundColor = UIColor.clear
        imgWork.backgroundColor = UIColor.clear
        imgPersonal.backgroundColor = UIColor.clear
        imgOther.backgroundColor = UIColor.clear
        
        if(sender.tag == 1){
            imgHome.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            strType = "Home"
            
        }else if(sender.tag == 2){
            imgFriend.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            strType = "Friend"
            
            
        }else if(sender.tag == 3){
            imgWork.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            strType = "Work"
            
        }else if(sender.tag == 4){
            imgPersonal.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            strType = "Personal"
            
        }else if(sender.tag == 5){
            imgOther.backgroundColor = hexStringToUIColor(hex: primaryDarkColor)
            strType = "Others"
            
        }
    }
    
    @IBAction func actionOnSaveAddress(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(validationForAddNewAddress()){
            let dict = ["UserId": "0", "Name": "\(txtFullName.text!)", "MobileNumber": "\(txtMobile.text!)", "EmailId": "\(txtEmail.text!)", "Password": "", "ProfileImage": "", "HostUserId": "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", "Address": "\(txtLandMark.text!)", "AddressType": strType]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(dict) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print(jsonString)
                    APIAddNewAddress(Jsondict: jsonString)
                }
            }
        }
    }
    
    
}
// MARK:
// MARK:- Validation

extension AddNewAddressVC {
    func validationForAddNewAddress() -> Bool {
        if(txtFullName.text?.count == 0){
            addErrorMessage(textView: txtFullName, message: alert_UserName)
            return false
        }
        else if (txtEmail.text?.count == 0){
            addErrorMessage(textView: txtEmail, message: alert_Email)
            return false
            
        }
            
        else if !(txtEmail.text!.isValidEmailAddress()){
            addErrorMessage(textView: txtEmail, message: alert_EmailValid)
            return false
            
        }
        else if(txtMobile.text?.count == 0){
            addErrorMessage(textView: txtMobile, message: alert_MobileNumber)
            
            return false
        }else if(txtMobile.text!.count < 10){
            
            addErrorMessage(textView: txtMobile, message: alert_MobileNumberValid)
            return false
            
        }
        else if (txtLandMark.text?.count == 0){
            addErrorMessage(textView: txtLandMark, message: alert_LandMark)
            return false
        }
        
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK: -
// MARK: - API Calling


extension AddNewAddressVC  {
    
    func APIAddNewAddress(Jsondict : String) {
        if(isInternetAvailable()){
            self.btnSave.startAnimation()
            WebServiceClass.getRequestWithHeadersWithImage(JsonData: Jsondict,JsonDataKey: "UserRegistrationMdl", image: UIImage(), imageName: "", url: URL_AddUpdateUserRegistration) { (responce, status) in
                if(status){
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if(apiStatus == "True"){
                        self.btnSave.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
                            
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
                                let nc = NotificationCenter.default
                                nc.post(name: Notification.Name("NIM_AddNewAddress"), object: nil)
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else{
                        self.btnSave.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }
                }else{
                    self.btnSave.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
            
            
        }
        else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
            
        }
    }
}
