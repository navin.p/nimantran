//
//  NotificationVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/28/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import MessageUI
import CRNotifications

class NotificationVC: UIViewController {
    
    
    // MARK:
    // MARK:- Variable
    var strComeFrom = String()
    var dictNotiData = NSMutableDictionary()
    var aryRecipientList = NSMutableArray()
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var viewHold: UIView!
    
    // MARK:
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
                      let aps = (dictNotiData.value(forKey: "aps")as! NSDictionary)
                      let alert = (aps.value(forKey: "alert")as! NSDictionary)
                      let strTitle = "\(alert.value(forKey: "title")!)"
                      let strbody = "\(alert.value(forKey: "body")!)"
        
        if(strTitle == "Send Reminder Message Notification"){
            var actualNotificationData = NSMutableDictionary()
            
            
            
            let string = (dictNotiData.value(forKey: "JsonObject")as! String)
            do {
                let aryJson = try convertToDictionary(from: string)
                print(aryJson) // prints: ["City": "Paris"]
                actualNotificationData = (aryJson.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        let strLoginUserID = "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)"
                        let strEventUserID =  "\(actualNotificationData.value(forKey: "EventId")!)"
                        let dict = NSMutableDictionary()
                       dict.setValue("\(strEventUserID)", forKey: "EventId")
                        if(strLoginUserID == strEventUserID){
                            dict.setValue("0", forKey: "InviteByUserId")
                        }else{
                            dict.setValue("\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", forKey: "InviteByUserId")
                        }
                        getGuestListByUserID(dict: dict, tag: 0)
            } catch {
                print(error)
            }
            
            
       
        }else{
            
            
            let alert = UIAlertController(title: strTitle, message: strbody, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if(self.strComeFrom == "DashBoard"){
                           self.navigationController?.popViewController(animated: true)
                       }else{
                           let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                           self.navigationController?.pushViewController(testController, animated: false)
                       }
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
 
    }
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actiononBack(_ sender: UIButton) {
        if(strComeFrom == "DashBoard"){
            self.navigationController?.popViewController(animated: true)
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
        
        
    }
    // MARK:
    // MARK:- Calling API
    
    
    func getGuestListByUserID(dict : NSDictionary , tag : Int) {
        if(isInternetAvailable()){
            
            let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
            
            WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GetEventAddressBookNimantran) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if(status){
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if(apiStatus == "True"){
                        
                        let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                        self.aryRecipientList = NSMutableArray()
                        self.aryRecipientList = arryData.mutableCopy()as! NSMutableArray
                        self.openNextController()
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                    }
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                    
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
            
        }
    }
    // MARK:
    // MARK:- Extra Function
    func openNextController()  {
        
        if(self.aryRecipientList.count != 0){
            
            let alert = UIAlertController(title: "", message: alert_SMS,preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
                
                self.openMessageComposser()
            }))
            alert.addAction(UIAlertAction(title: "Cancel",
                                          style: UIAlertAction.Style.destructive,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
            }))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertRecipientAdd, dismissDelay: 2)
            
            
        }
        
        
    }
    func openMessageComposser() {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            
            var strNumber = ""
            for item in self.aryRecipientList {
                strNumber = (item as AnyObject).value(forKey: "MobileNumber") as! String + ","
            }
            if(strNumber.count != 0){
                strNumber = String(String(strNumber).dropLast())
            }
            let recipientsArray = strNumber.components(separatedBy: ",")
            controller.recipients = recipientsArray
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertCalling, dismissDelay: 3)
            
        }
    }
}
// MARK:
// MARK:- UICollectionView
extension NotificationVC : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
        if(strComeFrom == "DashBoard"){
                   self.navigationController?.popViewController(animated: true)
               }else{
                   let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                   self.navigationController?.pushViewController(testController, animated: false)
               }
    }
}
