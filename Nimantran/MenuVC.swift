//
//  MenuVC.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnTransprant: UIButton!
    
    weak var handleDrawerView: DrawerScreenDelegate?
    var aryMenu = NSMutableArray()
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTransprant.isHidden = true
        aryMenu = [["title":"Home","image":"nav1_my_account"],["title":"My Address Book","image":"nav2_my_address_book"],["title":"Add a Co-Host","image":"nav2_my_address_book"],["title":"Sent","image":"nav3_sent"],["title":"RSVP","image":"nav5_favorites"],["title":"About App","image":"nav7_about_app"],["title":"Term's of use","image":"nav8_termsandcondition"],["title":"Help and Support","image":"nav9_help_support"],["title":"Change Password","image":"nav10_faq"]]
        
      //  aryMenu = [["title":"My Account","image":"nav1_my_account"],["title":"My Address Book","image":"nav2_my_address_book"],["title":"Sent","image":"nav3_sent"],["title":"Received","image":"nav4_revieved"],["title":"RSVP message","image":"nav5_favorites"],["title":"Notification","image":"nav6_notification"],["title":"About App","image":"nav7_about_app"],["title":"Term's of use","image":"nav8_termsandcondition"],["title":"Help and Support","image":"nav9_help_support"],["title":"Change Password","image":"nav10_faq"]]

        
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvList.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        UIView.animate(withDuration: 0.5) {
            self.tvList.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.btnTransprant.isHidden = false
        }
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        handleDrawerView?.refreshDrawerScreen(strType: "", tag: 0)
        UIView.animate(withDuration: 0.5) {
            self.tvList.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
}
// MARK:
// MARK:- UITableViewDataSource
extension MenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 1
        }else if (section == 1){
            return aryMenu.count
            
        }else{
            return 1
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tvList.dequeueReusableCell(withIdentifier: "Profile", for: indexPath as IndexPath) as! ProfileCell
            
            
            cell.simpleLabel.text = "\(getLogInData(key: "Nim_LoginData").value(forKey: "Name")!)" 
            //cell.subsimpleLabel.text = "\(getLogInData(key: "Nim_LoginData").value(forKey: "EmailId")!)"
            cell.subsimpleLabel.text = ""
            let urlImage = "\(URL_profileimageDownLoad)\(getLogInData(key: "Nim_LoginData").value(forKey: "ProfileImage")!)"
                cell.avatarImageView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    //   print(url ?? 0)
                }, usingActivityIndicatorStyle: .gray)
            return cell
        }else if (indexPath.section == 1){
            let cell = tvList.dequeueReusableCell(withIdentifier: "Menu", for: indexPath as IndexPath) as! MenuCell
            let dict = aryMenu.object(at: indexPath.row)as! NSDictionary
            cell.lblMenu.text = dict["title"]as? String
            cell.imgMenu.image = UIImage(named: "\(dict["image"]!)")
            return cell
        }
        else {
            let cell = tvList.dequeueReusableCell(withIdentifier: "LogOut", for: indexPath as IndexPath) as! MenuCell
            cell.btnlogOut.layer.cornerRadius = 15.0
            cell.btnVersion.setTitle("Version - \(app_Version)", for: .normal)
            cell.btnVersion.addTarget(self, action: #selector(Version), for: .touchUpInside)
            cell.btnlogOut.addTarget(self, action: #selector(LogOut), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0){
            
            self.dismiss(animated: false) {
                          self.handleDrawerView?.refreshDrawerScreen(strType: "Profile", tag: indexPath.row)
                      }
        }
        else if (indexPath.section == 1){
            let dict = aryMenu.object(at: indexPath.row)as! NSDictionary
            self.dismiss(animated: false) {
                self.handleDrawerView?.refreshDrawerScreen(strType: (dict["title"]as? String)!, tag: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 175
        }else if (indexPath.section == 1){
            return 50
        }
        else {
            return 85
        }
    }
    @objc func LogOut() {
        self.dismiss(animated: false) {
            self.handleDrawerView?.refreshDrawerScreen(strType: "Log Out", tag: 99)
        }
    }
    @objc func Version() {
        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:"App Version : \(app_Version)\nDate : \(app_VersionDate)\n\(app_VersionSupport)" , viewcontrol: self)
        
    }
}
// MARK:
// MARK:- ProfileCell
class ProfileCell: UITableViewCell {
    
    @IBOutlet  weak var avatarImageView: UIImageView!
    @IBOutlet  weak var simpleLabel: UILabel!
    @IBOutlet  weak var subsimpleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
           self.selectionStyle = .none
       }
}
// MARK:
// MARK:- MenuCell
class MenuCell: UITableViewCell {
    
    @IBOutlet  weak var lblMenu: UILabel!
    @IBOutlet  weak var imgMenu: UIImageView!
    @IBOutlet  weak var btnlogOut: UIButton!
    @IBOutlet  weak var btnVersion: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
           self.selectionStyle = .none
       }
    
}
//MARK:
//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}
