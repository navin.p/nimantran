//
//  AddEventVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/19/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import TransitionButton

class AddEventVC: UIViewController {
    
    var aryEventImage = NSMutableArray()
    var aryEventType = NSMutableArray()

    var imagePicker = UIImagePickerController()
     var lat = "0.0"
    var long = "0.0"
    var time = ""
    
    @IBOutlet weak var txtEventName: ACFloatingTextfield!
    @IBOutlet weak var txtHostName: ACFloatingTextfield!
    @IBOutlet weak var txtAboutEvent: ACFloatingTextfield!
    @IBOutlet weak var txtDate: ACFloatingTextfield!
    @IBOutlet weak var txtTime: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEventType: ACFloatingTextfield!

    @IBOutlet weak var txtAddress: KMPlaceholderTextView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var btnAddEvent: TransitionButton!
    @IBOutlet weak var btnWithQRCode: UIButton!
    @IBOutlet weak var btnWithOutQRCode: UIButton!

    // MARK:
       // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        txtAddress.layer.cornerRadius = 5.0
//        txtAddress.layer.borderWidth = 1.0
        txtAddress.layer.borderColor = UIColor.lightGray.cgColor
    //    txtAddress.textColor = UIColor.lightGray
        txtAddress.text = "Event Address..."
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        btnWithQRCode.setImage(UIImage(named: "uncheck"), for: .normal)
        btnWithOutQRCode.setImage(UIImage(named: "check"), for: .normal)
       
        self.getEventType_API()
    }
    override func viewWillAppear(_ animated: Bool) {
        if(txtAddress.text == "Event Address..."){
          txtAddress.textColor = UIColor.lightGray
        }else{
            txtAddress.textColor = UIColor.black
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {

                   let text = textField.text ?? ""

                   let trimmedText = text.trimmingCharacters(in: .whitespaces)

                   textField.text = trimmedText
              if(txtMobile == textField){
                textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
                 textField.text = textField.text?.replacingOccurrences(of: " ", with: "")
            }
       }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnWithQRCode(_ sender: UIButton) {
           self.view.endEditing(true)
             btnWithQRCode.setImage(UIImage(named: "check"), for: .normal)
             btnWithOutQRCode.setImage(UIImage(named: "uncheck"), for: .normal)
        
       }
    @IBAction func actionOnWithOutQRCode(_ sender: UIButton) {
              self.view.endEditing(true)
                btnWithQRCode.setImage(UIImage(named: "uncheck"), for: .normal)
                btnWithOutQRCode.setImage(UIImage(named: "check"), for: .normal)
           
          }
    @IBAction func actionOnAddress(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func actionOnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
        vc.strMode = "Date"
        vc.strTag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegateDateTime = self
        self.present(vc, animated: true, completion: {})
    }
    @IBAction func actionOnTime(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
               vc.strMode = "Time"
               vc.strTag = 2
               vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
               vc.modalTransitionStyle = .coverVertical
               vc.delegateDateTime = self
               self.present(vc, animated: true, completion: {})
    }
    @IBAction func actionOnEventType(_ sender: UIButton) {
           self.view.endEditing(true)
           let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
                vc.strTag = 1
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .coverVertical
                vc.delegate = self
                if aryEventType.count != 0{
                    vc.aryList = aryEventType
                    self.present(vc, animated: true, completion: {})
                }else{
                   CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertDataNotFound, dismissDelay: 2)
                }
        
       }
    
    @IBAction func actionOnAddEvent(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(validationForAddEvent()){
            if(isInternetAvailable()){
                var strNameOfimages =  ""
                for item in aryEventImage {
                                      let dict = (item as AnyObject) as! NSDictionary
                                      let imageName = dict.value(forKey: "imageName") as! String
                                      strNameOfimages =  strNameOfimages + imageName + ","
                                  }
                if(strNameOfimages != ""){
                    strNameOfimages = String(String(strNameOfimages).dropLast())
                }
                
//                var dateFormate = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//                var onlydateFormate = "yyyy-MM-dd"
//                var onlyTimeFormate = "HH:mm:ss.SSS"
             //   var sendDate = "\(txtDate.text!) " + "'T'" + "\(self.time)"

                var strWithQRCode = ""
                if  btnWithQRCode.currentImage == UIImage(named: "check") {
                    strWithQRCode = "True"
                }else{
                    strWithQRCode = "False"
                }
                
                let dict = ["EventId": "0", "UserId": "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", "EventType": "\(txtEventType.text!)", "EventName": "\(txtEventName.text!)", "AboutEvent": "\(txtAboutEvent.text!)", "EventAddress": "\(txtAddress.text!)", "EventLatitude": "\(self.lat)", "EventLongitude": "\(self.long)", "EventFromDate": "\(txtDate.text!)", "EventToDate": "\(self.time)", "ContactNumber": "\(txtMobile.text!)", "EventImages": "\(strNameOfimages)", "IsActive": "True", "IsGenerateQRCode": "\(strWithQRCode)"]
                         let encoder = JSONEncoder()
                         if let jsonData = try? encoder.encode(dict) {
                             if let jsonString = String(data: jsonData, encoding: .utf8) {
                                 print(jsonString)
                                 getRequestWithHeadersWithMultipleImage(dict: jsonString, aryImage: self.aryEventImage)
                             }
                         }
                
                
                
            }else{
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)

            }
        }
        
    }
    @IBAction func actionOnBrowse(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let actioncamera = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction!) in
            
            self.imagePicker = UIImagePickerController()

                           if UIImagePickerController.isSourceTypeAvailable(.camera){
                               print("Button capture")
                               
                               self.imagePicker.delegate = self
                               self.imagePicker.sourceType = .camera
                               self.imagePicker.allowsEditing = false
                                self.present(self.imagePicker, animated: true, completion: nil)
                           }else{
                            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)

            }
        }
        actioncamera.setValue(UIImage(named: "camera-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        
        alertController.addAction(actioncamera)
        
        
        let actionGallery = UIAlertAction(title: "Gallery", style: .default) { (action: UIAlertAction!) in
            self.imagePicker = UIImagePickerController()
                           if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                               print("Button capture")
                               self.imagePicker.delegate = self
                               self.imagePicker.sourceType = .savedPhotosAlbum
                               self.imagePicker.allowsEditing = false
                                self.present(self.imagePicker, animated: true, completion: nil)
                           }else{
                            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)

            }
        }
        actionGallery.setValue(UIImage(named: "gallery-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        alertController.addAction(actionGallery)
        let actionDismiss = UIAlertAction(title: "Dismiss", style: .destructive) { (action: UIAlertAction!) in
            
         }
         alertController.addAction(actionDismiss)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    

    
    
}

//MARK:-
//MARK:- PopUpDelegate

extension AddEventVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        txtEventType.text =  "\(dictData.value(forKey: "EventType")!)"
      //  txtEventType.tag =  Int("\(dictData.value(forKey: "locationareaid")!)")!
    }
}

//MARK:-
//MARK:- AddressFromMapScreenDelegate

extension AddEventVC: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
                self.txtAddress.text = (dictData.value(forKey: "address")as! String)
        self.lat = (dictData.value(forKey: "lat")as! String)
                 self.long = (dictData.value(forKey: "long")as! String)
        if ( self.txtAddress.text == "Event Address..." ||  self.txtAddress.text == "")  {
                    self.txtAddress.text = "Event Address..."
                    self.txtAddress.textColor = UIColor.lightGray
               }
        
    }
    
    
}
//MARK:-
//MARK:- ---------Date Time Selection Delegate
extension AddEventVC : DateTimeDelegate{
    func getDataFromDelegate(time: Date, tag: Int) {
        let dateFormatter = DateFormatter()
        if(tag == 1){ // For Date
            dateFormatter.dateFormat = "MM/dd/yyyy"
            self.txtDate.text = "\(dateFormatter.string(from: time))"

        }else if (tag == 2){ // for time
            dateFormatter.dateFormat = "hh:mm a"
            self.txtTime.text = "\(dateFormatter.string(from: time))"
            
            dateFormatter.dateFormat = "\(onlyTimeFormate)"
            self.time = "\(dateFormatter.string(from: time))"
            
        }
    }
    
}
extension UIImage {
    
    func imageWithSize(scaledToSize newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
// MARK: -
// MARK: -UINavigationControllerDelegate
extension AddEventVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
            let dict = NSMutableDictionary()
            dict.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
            dict.setValue(getUniqueString() + ".png", forKey: "imageName")
            self.aryEventImage.add(dict)
        print(aryEventImage)
        self.collection.dataSource = self
        self.collection.delegate = self

            self.collection.reloadData()
    }
}
// MARK:
// MARK:- UICollectionView
extension AddEventVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryEventImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventNewCell", for: indexPath as IndexPath) as! eventNewCell
        let dict  = aryEventImage.object(at: indexPath.row)as! NSDictionary
        
        cell.event_Image.image = (dict.value(forKey: "image")!) as? UIImage
        cell.event_Image.layer.cornerRadius = 5
             cell.event_Image.contentMode = .scaleToFill
         cell.btnDelete.tag = indexPath.row
         cell.btnDelete.addTarget(self, action: #selector(buttonDelete), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    @objc func buttonDelete(_ sender: UIButton!) {
          let alert = UIAlertController(title: alertMessage, message: alertEventImageDelete, preferredStyle: .actionSheet)
                           alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                            self.aryEventImage.removeObject(at: sender.tag)
                            self.collection.reloadData()
                           }))
                           
                           alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                               print("User click Dismiss button")
                           }))
                           
                           self.present(alert, animated: true, completion: {
                           })
        
       }
}

// MARK:
// MARK:- UICollectionViewCell
class eventNewCell: UICollectionViewCell {
    @IBOutlet weak var event_Image: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!

}
//// MARK:
//// MARK:- UITextViewDelegate
//extension AddEventVC : UITextViewDelegate{
//    func textViewDidBeginEditing(_ textView: UITextView) {
//
//        if txtAddress.textColor == UIColor.lightGray {
//            txtAddress.text = ""
//            txtAddress.textColor = UIColor.black
//        }
//    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//
//        if (txtAddress.text == "Event Address..." || txtAddress.text == "")  {
//            txtAddress.text = "Event Address..."
//            txtAddress.textColor = UIColor.lightGray
//        }
//    }
//}
// MARK:
// MARK:- Validation

extension AddEventVC {
    func validationForAddEvent() -> Bool {
        if(txtAddress.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EventAddress, viewcontrol: self)
            return false
        }
            
        else if(txtEventName.text?.count == 0){
            addErrorMessage(textView: txtEventName, message: alert_EventName)
            return false
        }
            
            
        else if(txtEventType.text?.count == 0){
            addErrorMessage(textView: txtEventType, message: alert_Event_Type)
            
            return false
        }
            
        else if(txtAboutEvent.text?.count == 0){
            addErrorMessage(textView: txtAboutEvent, message: alert_EventAbout)
            
            return false
        }
            
        else if(txtHostName.text?.count == 0){
            addErrorMessage(textView: txtHostName, message: alert_EventHostNAme)
            
            return false
        }
        else if(txtDate.text?.count == 0){
            addErrorMessage(textView: txtDate, message: alert_EventDate)
            
            return false
        }
        else if(txtTime.text?.count == 0){
            addErrorMessage(textView: txtTime, message: alert_EventTime)
            
            return false
        }
        else if(txtMobile.text?.count == 0){
            addErrorMessage(textView: txtMobile, message: alert_MobileNumber)
            
            return false
        }
        else if(txtMobile.text!.count < 10){
            
            addErrorMessage(textView: txtMobile, message: alert_MobileNumberValid)
            return false
            
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK: -
// MARK: - API Calling


extension AddEventVC  {
    
    
    
    func getRequestWithHeadersWithMultipleImage(dict : String , aryImage : NSMutableArray) {
        if(isInternetAvailable()){
            self.btnAddEvent.startAnimation()
            WebServiceClass.getRequestWithHeadersWithMultipleImage(JsonData: dict,JsonDataKey: "EventMdl", imageArray: aryImage, url: URL_AddUpdateEvent) { (responce, status) in
                if(status){
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if(apiStatus == "True"){
                        self.btnAddEvent.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
                 
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
                                let nc = NotificationCenter.default
                                nc.post(name: Notification.Name("NIM_AddEvent"), object: nil)
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else{
                        self.btnAddEvent.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }
                }else{
                    self.btnAddEvent.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                         CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
          
            
        }
        else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
            
        }
    }

    
    func getEventType_API() {
        
        if(isInternetAvailable()){
            
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            let urlGetEventType = "\(URL_GetEventType)"
            WebServiceClass.getRequestWithHeadersGET(url: urlGetEventType) { (responce, status) in
                loader.dismiss(animated: true, completion: nil)
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    //let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        self.aryEventType = NSMutableArray()
                        self.aryEventType = (((responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")!)as! NSArray).mutableCopy()as! NSMutableArray
                        
                        
                    }else{
                       // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                    }
                }else{
                    
                   // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                }
            }
        }else{
          //  CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    
}
