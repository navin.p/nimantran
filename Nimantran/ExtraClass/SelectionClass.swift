//
//  SelectionClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//


// tag 1,2,3 Country , State , City
// tag = 4 for state vehicle search
// tag = 5 for Radius
// tag = 6,7 for ReportAnIncident
// tag = 8 for ReportLostArticle
// tag = 9 for FareType
// tag = 10 for OfficialSearch
// tag = 11 for ReportAnIncident
// tag = 12 for Area CitizenEYE

import UIKit

class SelectionClass: UIViewController {
    
    // MARK: - ----IBOutlet

    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    open weak var delegate:PopUpDelegate?

    // MARK: - ----Variable
    
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var strTitle = String()
    var strTag = Int()
    // MARK: - ----Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aryForListData = aryList
        tvlist.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - ----IBAction

    @IBAction func actionOnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true) {
            
        }
    }

}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SelectionClass : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath as IndexPath) as! SelectionCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        if(self.strTag == 1){
            cell.lblTitle.text = "\(dict.value(forKey: "EventType")!)"

        }
     
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)

        let dict = aryList.object(at: indexPath.row)as? NSDictionary
        self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
        self.dismiss(animated: false) {}
    }
    
    
}
// MARK: - ----------------SlectionCell
// MARK: -
class SelectionCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  SelectionClass : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        let resultPredicate = NSPredicate(format: "EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@", argumentArray: [Searching, Searching , Searching , Searching, Searching , Searching , Searching , Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}

//MARK: -------Protocol

protocol PopUpDelegate : class{
    func getDataFromPopupDelegate(dictData : NSDictionary ,tag : Int)
}
