//
//  ConstantClass.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/5/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//
import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import AVKit



//MARK: Common use in app
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var globleLogInData = NSMutableDictionary()

var primaryDarkColor = "6C0C0E"
var primaryColor = "ffaf10"
var primaryG1 = "FAAF10"
var primaryG2 = "FACC00"
var primaryG3 = "13aa8f"
var whiteColor = "F9FFFF"


var primaryDarkG1 = "000000"
var primaryDarkG2 = "808080"


var strLat = "0.0"
var strLong = "0.0"

var app_Name = "Nimantran"
var app_Version : String = "1.0.0"
var app_VersionDate : String = ""
var app_VersionSupport : String = "Requires iOS 11.0 or later & Compatible with iPhone."
var application_ID : String = "https://itunes.apple.com/in/app/aahar-daan/id1164477317?mt=8"
var Platform = "iPhone"
var dateFormate = "yyyy-MM-dd'T'HH:mm:ss.SSS"
var dateFormate1 = "yyyy-MM-dd'T'HH:mm:ss.SS"
var dateFormate2 = "yyyy-MM-dd'T'HH:mm:ss"


var onlydateFormate = "yyyy-MM-dd"
var onlyTimeFormate = "HH:mm:ss.SSS"


var alertMessage = "Alert!"
var alertMsg = "Message"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Sorry , Data is not Available!"
var alertSomeError = "Somthing went wrong please try again!"
var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout ?"
var alertEventDelete = "Are you sure want to delete event ?"
var alertAddressDelete = "Are you sure want to delete address ?"
var alertEventImageDelete = "Are you sure want to delete event image ?"
var alertDelete = "Are you sure want to delete ?"
var alertEventRemove = "Are you sure want to remove event ?"
var alertRecipientAdd = "Please add recipient first."


var alert_MobileNumber = "Mobile number is required!"
var alert_MobileNumberValid = "Mobile number is invalid!"
var alert_Password = "Password is required!"
var alert_CPassword = "Confirm password is required!"
var alert_Password_CPassword = "Confirm password is required!"
var alert_UserName = "User name is required!"
var alert_required = "required!"
var alert_EmailValid = "Email address is invalid!"

var alert_Email = "Email address is required!"
var alert_OldPassword = "Old password is required!"
var alert_NewPassword = "New password is required!"

var alert_Terms = "Please accept terms and condition."


var alert_SMS = "Are you sure want to send an SMS?\nTarrif chargs will be applied on sending."
var alertAddressNotFound = "Contact is not Available!"
var alert_LandMark = "Landmark is required!"


var alert_EventAddress = "Event address is required!"
var alert_EventName = "Event name is required!"
var alert_Event_Type = "Event type is required!"

var alert_EventAbout = "About event is required!"
var alert_EventHostNAme = "Event host name is required!"
var alert_EventDate = "Event date is required!"
var alert_EventTime = "Event time is required!"

var alert_ReminderDate = "Event reminder date is required!"
var alert_ReminderTime = "Event reminder time is required!"
var alert_NumberOfMember = "Number of member is required!"

var alert_Reason = "Reason is required!"



//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}



func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


func getgradientLayerImage(bounds : CGRect , height : CGFloat) -> UIImage {
    let gradientLayer = CAGradientLayer()
    var updatedFrame = bounds
    updatedFrame.size.height += height
    gradientLayer.frame = updatedFrame
    gradientLayer.colors = [hexStringToUIColor(hex: primaryG1).cgColor, hexStringToUIColor(hex: primaryG2).cgColor] // start color and end color
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0) // Horizontal gradient start
    gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0) // Horizontal gradient end
    UIGraphicsBeginImageContext(gradientLayer.bounds.size)
    gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
    
    
}

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}
func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}
func getgradientLayer(bounds : CGRect) -> CAGradientLayer {
    
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = bounds
    gradientLayer.colors = [hexStringToUIColor(hex: primaryG1).cgColor, hexStringToUIColor(hex: primaryG2).cgColor]
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
    gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
    gradientLayer.locations = [0.0, 1.0]
    return gradientLayer
}

@IBDesignable
class GradientView: UIView {
    @IBInspectable var startColor:   UIColor = hexStringToUIColor(hex: primaryG1)
        
        { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = hexStringToUIColor(hex: primaryG2)
        { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override public class var layerClass: AnyClass { CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}
extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}
extension ACFloatingTextfield {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    @IBInspectable var borderWidth: Float = 0.0
    @IBInspectable var borderColor: UIColor = UIColor.gray
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = CGFloat(borderWidth)
    }
    
}

extension String {
    
    func isValidEmailAddress() -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"
            + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
            + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
            + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
            + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
            + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
            + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
func getJson(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func convertToDictionary(from text: String) throws -> NSArray {
    guard let data = text.data(using: .utf8) else { return NSArray() }
    let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
    return anyResult as? NSArray ?? NSArray()
}

func getLogInData(key : String) -> NSDictionary {
    if(nsud.value(forKey: key) != nil){
        return nsud.value(forKey: key)as! NSDictionary
    }
    return NSDictionary()
}
func getUniqueString() -> String {
    let df = DateFormatter()
    df.dateFormat = "yyyyMMddhhmmss"
    return df.string(from: Date())
}

func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}


func loader_Show(controller: UIViewController , strMessage : String , title : String ,style : UIAlertController.Style) -> UIAlertController {
    let alert = UIAlertController(title: title, message:strMessage, preferredStyle: style)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    if #available(iOS 13.0, *) {
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
    } else {
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
    }
    loadingIndicator.startAnimating();
    alert.view.addSubview(loadingIndicator)
    
    controller.present(alert, animated: false, completion: nil)
    return alert
}


extension NSMutableDictionary {
    
    
    func removeNullFromDict () -> NSMutableDictionary
    {
        let dic = self;
        
        for (key, value) in self {
            
            let val : NSObject = value as! NSObject;
            if(val.isEqual(NSNull()))
            {
                dic.setValue("", forKey: (key as? String)!)
            }
            else
            {
                dic.setValue(value, forKey: key as! String)
            }
            
        }
        
        return dic;
    }
}
extension NSDictionary {
    
    
    func nullKeyRemoval() -> NSDictionary {
        var dict = (self as! Dictionary<String,Any>)
        
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
            ((dict as NSDictionary).mutableCopy()as! NSMutableDictionary).setValue("", forKey: key)
        }
        
        return dict as NSDictionary
    }
}
func showLocationAccessAlertAction(viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
    alert.addAction(UIAlertAction (title: "Go to Setting", style: .default, handler: { (nil) in
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }))
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}





func getDateTime(FormateDate: String , FormateTime : String , strdate: String) -> String {
    let strSendDate = ""
    let dateFormatter = DateFormatter()
    let strDateTimeString = strdate
//    if(strdate.split(separator: ".").count != 0){
//        strDateTimeString = String(strdate.split(separator: ".")[0])
//    }
    
      dateFormatter.dateFormat = "\(dateFormate)"
    if(checkFormaet(formatter: dateFormatter, strDate: strDateTimeString)){
       return getFinalDate(formate: dateFormate, FormateDate: FormateDate, FormateTime: FormateTime, strdate: strDateTimeString)
    }
   
    dateFormatter.dateFormat = "\(dateFormate1)"
    if(checkFormaet(formatter: dateFormatter, strDate: strDateTimeString)){
        return getFinalDate(formate: dateFormate1, FormateDate: FormateDate, FormateTime: FormateTime, strdate: strDateTimeString)

       }
    dateFormatter.dateFormat = "\(dateFormate2)"
    if(checkFormaet(formatter: dateFormatter, strDate: strDateTimeString)){
              return getFinalDate(formate: dateFormate2, FormateDate: FormateDate, FormateTime: FormateTime, strdate: strDateTimeString)
      }
  return strSendDate
}
func getFinalDate(formate : String , FormateDate : String, FormateTime : String , strdate: String) -> String{
    let dateFormatter = DateFormatter()
    var strSendDate = ""
      dateFormatter.dateFormat = formate
    let date = dateFormatter.date(from:strdate)!
      dateFormatter.dateFormat = FormateDate
      strSendDate = "\(dateFormatter.string(from: date))"
      dateFormatter.dateFormat = FormateTime
      strSendDate = strSendDate + "," + "\(dateFormatter.string(from: date))"
      return strSendDate
    
}
func checkFormaet(formatter : DateFormatter , strDate : String)-> Bool{
    guard let _ = formatter.date(from: strDate)else {
                
        return false
         }
    return true

}
