//
//  WebClass.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/12/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import WebKit

class WebClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var urlString = ""
    var titleString = ""
    
    // MARK:
    // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = NSURL(string: urlString)
        let request = NSURLRequest(url: url! as URL)
        lbltitle.text = titleString
        // init and load request in webview.
        webView.navigationDelegate = self
        webView.load(request as URLRequest)
        
    }
    
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension WebClass : WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        indicator.isHidden = true
        
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        indicator.isHidden = false
        print("Strat to load")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
               indicator.isHidden = true
    }
}
