//
//  CohostListVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/21/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import UserNotifications


class CohostListVC: UIViewController {

    
    // MARK:
          // MARK:- IBOutlet
          @IBOutlet weak var viewHold: UIView!
    @IBOutlet weak var lblTitle: UILabel!

          @IBOutlet weak var txtSearch: UISearchBar!
          var aryList = NSMutableArray()
          var arySelectedList = NSMutableArray()
          var strErrorMessage  = ""
          var dictEventData = NSDictionary()

          @IBOutlet private weak var tableView: UITableView! {
              didSet {
                  tableView.coverableCellsIdentifiers = cellsIdentifiers
                  tableView.tableFooterView = UIView()
                  tableView.estimatedRowHeight = 200
              }
          }
          private var numberOfSections = 0
          private var numberOfRows = 0
          private var loadingPlaceholderView = LoadingPlaceholderView(){
              didSet{
                  do {
                      loadingPlaceholderView.gradientColor = .white
                      loadingPlaceholderView.backgroundColor = .white
                  }
              }
          }
          var refreshControl = UIRefreshControl()
          
          private var cellsIdentifiers = [
              "ReminderCell",
              "ReminderCell","ReminderCell",
              "ReminderCell","ReminderCell",
              "ReminderCell"
          ]
       
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dictEventData = dictEventData.nullKeyRemoval()
        lblTitle.text = "\(dictEventData.value(forKey: "EventName")!)"
        txtSearch.delegate = self
            refreshControl.attributedTitle = NSAttributedString(string: "Get Co-Host list...")
            refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
            if(isInternetAvailable()){
                loadingPlaceholderView.cover(viewHold, animated: true)
                GetCoHost(eventID: "\(dictEventData.value(forKey: "EventId")!)", tagIndex: 0)
            }else{
                self.aryList = NSMutableArray()
                self.strErrorMessage = alertInternet
                self.tableView.reloadData()
            }
            tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.view.endEditing(true)
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
        }
        
        @objc func refresh(sender:AnyObject) {
            txtSearch.text = ""
            GetCoHost(eventID: "\(dictEventData.value(forKey: "EventId")!)", tagIndex: 0)

        }
   
    // MARK:
       // MARK:- IBAction
       
       @IBAction func actionOnBack(_ sender: UIButton) {
           self.view.endEditing(true)
           self.navigationController?.popViewController(animated: true)
       }
    // MARK:
    // MARK:- API Calling
       func GetCoHost(eventID : String ,tagIndex : Int) {
           if(isInternetAvailable()){
               
               let GetCoHost = "\(URL_GetCoHost)EventId=\(eventID)"
               WebServiceClass.getRequestWithHeadersGET(url: GetCoHost) { (responce, status) in
                 self.loadingPlaceholderView.uncover(animated: true)
                 self.refreshControl.endRefreshing()
                   if(status){
                       let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                       let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                       if(apiStatus == "True"){
                          
                         let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                                            self.aryList = NSMutableArray()
                                            self.aryList = arryData.mutableCopy()as! NSMutableArray
                                            self.strErrorMessage = ""
                         DispatchQueue.main.async {
                             self.numberOfSections = 1
                             self.numberOfRows = self.aryList.count
                             self.tableView.reloadData()
                         }
                       }else{
                         self.aryList = NSMutableArray()
                         self.strErrorMessage = apiError
                         self.numberOfSections = 1
                         self.numberOfRows = self.aryList.count
                         self.tableView.reloadData()
                          // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                       }
                   }else{
                     self.aryList = NSMutableArray()
                     self.strErrorMessage = alertSomeError
                     self.numberOfSections = 1
                     self.numberOfRows = self.aryList.count
                     self.tableView.reloadData()
                      // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                   }
               }
           }else{
               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
           }
       }
}
// MARK: -
// MARK: - UISearchBarDelegate


extension CohostListVC: UISearchBarDelegate  {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
        self.view.endEditing(true)
    }
    
    
    
    func searchAutocomplete(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR Name contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
        }
        else{
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = self.aryList.mutableCopy() as! NSMutableArray
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(arySelectedList.count == 0){
            self.strErrorMessage = alertDataNotFound
            
        }
        
        DispatchQueue.main.async {
            self.numberOfSections = 1
            self.numberOfRows = self.arySelectedList.count
            self.tableView.reloadData()
        }
        
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CohostListVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "ReminderCell", for: indexPath as IndexPath) as! ReminderCell
        if(aryList.count != 0){
         
            
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            let strDateTime = "\(dict.value(forKey: "CreatedDate")!)"
            let dateFormatter = DateFormatter()

            //    2020-02-21T18:10:48.273

            dateFormatter.dateFormat = "\(dateFormate)"
            let date = dateFormatter.date(from:strDateTime)!
            dateFormatter.dateFormat = "MM/dd/yyyy"

            let strDate = "\(dateFormatter.string(from: date))"
            dateFormatter.dateFormat = "hh:mm a"
            let strTime = "\(dateFormatter.string(from: date))"
            
            cell.lbl_EventStatic.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            cell.lbl_EventStatic.frame = CGRect(x: 0, y: 3, width: 15, height: 60)
            cell.lbl_EventStatic.text = "Co-Host"
            cell.lbl_EventDate.text = "Co-Host Name : \(dict.value(forKey: "Name")!)"
            cell.lbl_EventTime.text = "Code : \(dict.value(forKey: "CoHostCode")!)\nTime : \(strDate) \(strTime)"
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
         
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }

    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }

 
    @objc func buttonCalling(_ sender: UIButton!) {
        
        if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
        }
        
        
    }
}
