//
//  SentDetailVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/24/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import LoadingPlaceholderView
import UserNotifications


class SentDetailVC: UIViewController {
    // MARK:
         // MARK:- IBOutlet
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK:
       // MARK:- IBAction
       
       @IBAction func actionOnBack(_ sender: UIButton) {
           self.view.endEditing(true)
           self.navigationController?.popViewController(animated: true)
       }


}
