//
//  WebServiceClass.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/12/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//


import UIKit
import Alamofire




var BaseURL = "http://nimantran.citizencop.org/"
var URL_aboutapp = "\(BaseURL)" + "aboutapp.aspx"
var URL_helpsupport = "\(BaseURL)" + "helpsupport.aspx"
var URL_termsofuse = "\(BaseURL)" + "termsofuse.aspx"

var URL_GetEvent = "\(BaseURL)" + "api/mobile/GetEvent"
var URL_GetLogin = "\(BaseURL)" + "api/mobile/GetLogin?"
var URL_ForgetPassword = "\(BaseURL)" + "api/mobile/ForgetPassword?"
var URL_AddUpdateUserRegistration = "\(BaseURL)" + "api/mobile/AddUpdateUserRegistration"

var URL_ChangePassword = "\(BaseURL)" + "api/mobile/ChangePassword?"
var URL_profileimageDownLoad = "\(BaseURL)" + "profileimage/"
var URL_eventimagesDownLoad = "\(BaseURL)" + "eventimages/"
var URL_GetAddressBook = "\(BaseURL)" + "api/mobile/GetAddressBook"
var URL_GetDeleteEvent = "\(BaseURL)" + "api/mobile/DeleteEvent?"
var URL_DeleteCoHostCode = "\(BaseURL)" + "api/mobile/DeleteCoHostCode?"

var URL_DeleteAddressBook = "\(BaseURL)" + "api/mobile/DeleteAddressBook?"

var URL_AddUpdateEvent = "\(BaseURL)" + "api/mobile/AddUpdateEvent"
var URL_GetEventReminder = "\(BaseURL)" + "api/mobile/GetEventReminder?"
var URL_AddUpdateEventReminder = "\(BaseURL)" + "api/mobile/AddUpdateEventReminder"

var URL_GetEventAddressBookNimantran = "\(BaseURL)" + "api/mobile/GetEventAddressBookNimantran?"
var URL_DeleteEventAddressBookNimantran = "\(BaseURL)" + "api/mobile/DeleteEventAddressBookNimantran?"

var AddUpdateEventAddressBookNimantran = "\(BaseURL)" + "api/mobile/AddUpdateEventAddressBookNimantran"
var AddDeviceRegistration = "\(BaseURL)" + "api/mobile/AddDeviceRegistration"
var URL_GenerateCoHost = "\(BaseURL)" + "api/mobile/GenerateCoHost"
var URL_GetCoHost = "\(BaseURL)" + "api/mobile/GetCoHost?"
var URL_UpdateCoHostCode = "\(BaseURL)" + "api/mobile/UpdateCoHostCode?"
var URL_UpdateSentInvitationCount = "\(BaseURL)" + "api/mobile/UpdateSentInvitationCount"
var URL_GetEventType = "\(BaseURL)" + "api/mobile/GetEventType"

var URL_UpdateRSVP = "\(BaseURL)" + "api/mobile/UpdateRSVP?"
var URL_ScanQRCodeUpdateRecipient = "\(BaseURL)" + "api/mobile/ScanQRCodeUpdateRecipient?"
var URL_address_book_format = "\(BaseURL)" + "/assets/images/address_book_format.xlsx"
var URL_ImportAddressBook = "\(BaseURL)" + "api/mobile/ImportAddressBook"

class WebServiceClass: NSObject , NSURLConnectionDelegate,XMLParserDelegate {
    
    class func callAPIFormDataWithImage(parameter:NSDictionary,url:String,image:UIImage,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void){
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if(withName.count != 0){
                
                let imgData = image.jpegData(compressionQuality: 0.2)!
                multipartFormData.append(imgData, withName: "fileset",fileName: "file.jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in parameter {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON {
                    response in
                    // debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            let dictdata = NSMutableDictionary.init()
                            
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,false)
            }
        }
    }
    
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "Result" == (k as AnyObject)as! String
                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"success")
                    }
                    else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                break
            case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    
    
    
    
    
    // MARK: - Convert String To Dictionary Function
    class func convertJsonStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    class func getRequestWithHeaders(dictJson:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            
            var theRequest = URLRequest(url: url)
            theRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            theRequest.httpMethod = "POST"
            guard let data = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                return
            }
            
            theRequest.httpBody = data
            
            
            Alamofire.request(theRequest)
                .responseJSON {
                    response in
                    // debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            let dictdata = NSMutableDictionary.init()
                            
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
            }
        }
        
    }
    class func getRequestWithHeadersWithArray(JsonArray:NSArray,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            
            var theRequest = URLRequest(url: url)
            theRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            theRequest.httpMethod = "POST"
            guard let data = try? JSONSerialization.data(withJSONObject: JsonArray, options: []) else {
                return
            }
            
            theRequest.httpBody = data
            
            
            Alamofire.request(theRequest)
                .responseJSON {
                    response in
                    // debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            let dictdata = NSMutableDictionary.init()
                            
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
            }
        }
        
    }
    
    
    class func getRequestWithHeadersGET(url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            
            var theRequest = URLRequest(url: url)
            theRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            theRequest.httpMethod = "GET"
            Alamofire.request(theRequest)
                .responseJSON {
                    response in
                    // debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            let dictdata = NSMutableDictionary.init()
                            
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
            }
        }
        
    }
    class func getRequestWithHeadersWithFile(JsonData:String,JsonDataKey:String, dataFile : Data , FileName : String ,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            
            Alamofire.upload(multipartFormData:{ multipartFormData in
                
                if(FileName.count != 0){
                    
                    
                  //  multipartFormData.append(dataFile, withName: "\(FileName)")
                    multipartFormData.append(dataFile, withName: "File", fileName: "\(FileName)", mimeType: "application/pdf/xls")
                }
                multipartFormData.append(Data(JsonData.utf8), withName: "\(JsonDataKey)")
            },
                             usingThreshold:UInt64.init(),
                             to:url,
                             method:.post,
                             headers:["Authorization": "auth_token"],
                             encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        
                                        if let data = response.result.value
                                        {
                                            let dictdata = NSMutableDictionary.init()
                                            
                                            dictdata.setValue(data, forKey: "data")
                                            OnResultBlock((dictdata) ,true)
                                            
                                        }else{
                                            let dic = NSMutableDictionary.init()
                                            dic.setValue("\(alertSomeError) ", forKey: "message")
                                            OnResultBlock(dic,false)
                                        }
                                    }
                                case .failure( _):
                                    let dic = NSMutableDictionary.init()
                                    dic.setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                     }
            })
        }
    }
    class func getRequestWithHeadersWithImage(JsonData:String,JsonDataKey:String, image : UIImage , imageName : String ,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            
            Alamofire.upload(multipartFormData:{ multipartFormData in
                
                if(imageName.count != 0){
                    multipartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "photo_path", fileName: "\(imageName)", mimeType: "image/jpeg")
                }
                multipartFormData.append(Data(JsonData.utf8), withName: "\(JsonDataKey)")
            },
                             usingThreshold:UInt64.init(),
                             to:url,
                             method:.post,
                             headers:["Authorization": "auth_token"],
                             encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        
                                        if let data = response.result.value
                                        {
                                            let dictdata = NSMutableDictionary.init()
                                            
                                            dictdata.setValue(data, forKey: "data")
                                            OnResultBlock((dictdata) ,true)
                                            
                                        }else{
                                            let dic = NSMutableDictionary.init()
                                            dic.setValue("\(alertSomeError) ", forKey: "message")
                                            OnResultBlock(dic,false)
                                        }
                                    }
                                case .failure( _):
                                    let dic = NSMutableDictionary.init()
                                    dic.setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                     }
            })
        }
    }
    class func getRequestWithHeadersWithMultipleImage(JsonData:String,JsonDataKey:String, imageArray : NSMutableArray ,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
          if let url = URL(string: url) {
              Alamofire.upload(multipartFormData:{ multipartFormData in
                  if(imageArray.count != 0){
                    for item in imageArray {
                        let dict = (item as AnyObject) as! NSDictionary
                        let image = dict.value(forKey: "image")as! UIImage
                        let imageName = dict.value(forKey: "imageName") as! String
                        multipartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "photo_path", fileName: "\(imageName)", mimeType: "image/jpeg")
                    }
                  }
                  multipartFormData.append(Data(JsonData.utf8), withName: "\(JsonDataKey)")
              },
                               usingThreshold:UInt64.init(),
                               to:url,
                               method:.post,
                               headers:["Authorization": "auth_token"],
                               encodingCompletion: { encodingResult in
                                  switch encodingResult {
                                  case .success(let upload, _, _):
                                      upload.responseJSON { response in
                                          
                                          if let data = response.result.value
                                          {
                                              let dictdata = NSMutableDictionary.init()
                                              
                                              dictdata.setValue(data, forKey: "data")
                                              OnResultBlock((dictdata) ,true)
                                              
                                          }else{
                                              let dic = NSMutableDictionary.init()
                                              dic.setValue("\(alertSomeError) ", forKey: "message")
                                              OnResultBlock(dic,false)
                                          }
                                      }
                                  case .failure( _):
                                      let dic = NSMutableDictionary.init()
                                      dic.setValue("\(alertSomeError) ", forKey: "message")
                                      OnResultBlock(dic,false)
                       }
              })
          }
      }
}


