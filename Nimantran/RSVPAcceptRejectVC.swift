//
//  RSVPAcceptRejectVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/25/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications


class RSVPAcceptRejectVC: UIViewController {
    // MARK:
       // MARK:- IBOutlet
       
       @IBOutlet weak var txtName: ACFloatingTextfield!
       @IBOutlet weak var txtMobile: ACFloatingTextfield!
       @IBOutlet weak var txtEmail: ACFloatingTextfield!
       @IBOutlet weak var txtNumberOfPerson: ACFloatingTextfield!
       @IBOutlet weak var btnSubmit: TransitionButton!
       @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    
    var dictEventData = NSMutableDictionary()
    // MARK:
         // MARK:- Life Cycle
      
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        lblTitle.text = "\(dictEventData.value(forKey: "EventName")!)"
        lblEventName.text = "\(dictEventData.value(forKey: "EventAddress")!)"
        lblEventDate.text = "Date: \(dictEventData.value(forKey: "EventFromDate")!)"
        
        txtName.text =  "\(getLogInData(key: "Nim_LoginData").value(forKey: "Name")!)"
        txtMobile.text =  "\(getLogInData(key: "Nim_LoginData").value(forKey: "MobileNumber")!)"
        txtEmail.text =  "\(getLogInData(key: "Nim_LoginData").value(forKey: "EmailId")!)"

    }
    override func viewWillLayoutSubviews() {
           for item in self.btnSubmit.layer.sublayers! {
               if item is CAGradientLayer {
                   item.removeFromSuperlayer()
               }
           }
           btnSubmit.layer.insertSublayer(getgradientLayer(bounds: btnSubmit.bounds), at: 0)
       }
    // MARK:
      // MARK:- IBAction
      @IBAction func actionOnBack(_ sender: TransitionButton) {
          self.view.endEditing(true)
       self.navigationController?.popViewController(animated: true)
      }
      @IBAction func actionOnAccept(_ sender: TransitionButton) {
             self.view.endEditing(true)
        if(validationForAccept() ){
            
            dictEventData = (dictEventData.mutableCopy()as! NSMutableDictionary).removeNullFromDict()

            AcceptInvitationByUserID(eventID: "\(dictEventData.value(forKey: "EventId")!)", RecipientUserId: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
        }

    }
    // MARK:
    // MARK:- Call API
    
    func AcceptInvitationByUserID(eventID : String ,RecipientUserId : String, tag : Int) {
          
     
        
          if(isInternetAvailable()){
            self.btnSubmit.startAnimation()

            let urlForgotPass = "\(URL_UpdateRSVP)RecipientUserId=\(RecipientUserId)&EventId=\(eventID)&IsAttend=True&NoOfPerson=\(txtNumberOfPerson.text!)&Description="
              WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
              self.btnSubmit.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
              if(status){
                              let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                              let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                              if(apiStatus == "True"){
                                 CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 2)
                                  
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: DashBoardVC.self) {
                                        _ =  self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                                
                                
                              }else{
                              
                                 CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                              }
                          }else{
                          
                              CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                          }
                }
            
              }
          }else{
         
             CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
          }
      }
    
  
}
// MARK:
// MARK:- Validation

extension RSVPAcceptRejectVC {
    func validationForAccept() -> Bool {
        if(txtNumberOfPerson.text?.count == 0){
            addErrorMessage(textView: txtNumberOfPerson, message: alert_NumberOfMember)
            
            return false
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension RSVPAcceptRejectVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
