//
//  RSVPRejectVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/25/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications
class RSVPRejectVC: UIViewController {
    
    
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var btnSubmit: TransitionButton!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!

    var dictEventData = NSMutableDictionary()
    
    
    
    // MARK:
    // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
          lblTitle.text = "\(dictEventData.value(forKey: "EventName")!)"
             lblEventName.text = "\(dictEventData.value(forKey: "EventAddress")!)"
             lblEventDate.text = "Date: \(dictEventData.value(forKey: "EventFromDate")!)"
        txtDescription.layer.borderColor = UIColor.darkGray.cgColor
        txtDescription.delegate = self
        lblCount.text =  "0/250"
        
    }
    override func viewWillLayoutSubviews() {
        for item in self.btnSubmit.layer.sublayers! {
            if item is CAGradientLayer {
                item.removeFromSuperlayer()
            }
        }
        btnSubmit.layer.insertSublayer(getgradientLayer(bounds: btnSubmit.bounds), at: 0)
    }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: TransitionButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnAccept(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(validationForReject() ){
            
            dictEventData = (dictEventData.mutableCopy()as! NSMutableDictionary).removeNullFromDict()
            
            AcceptInvitationByUserID(eventID: "\(dictEventData.value(forKey: "EventId")!)", RecipientUserId: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
        }
        
    }
    // MARK:
    // MARK:- Call API
    
    func AcceptInvitationByUserID(eventID : String ,RecipientUserId : String, tag : Int) {
        
        if(isInternetAvailable()){
            self.btnSubmit.startAnimation()
            
            let urlForgotPass = "\(URL_UpdateRSVP)RecipientUserId=\(RecipientUserId)&EventId=\(eventID)&IsAttend=False&NoOfPerson=&Description=\(txtDescription.text!)"
            WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                self.btnSubmit.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
                    if(status){
                        let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                        let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                        if(apiStatus == "True"){
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 2)
                            for controller in self.navigationController!.viewControllers as Array {
                                                              if controller.isKind(of: DashBoardVC.self) {
                                                                  _ =  self.navigationController!.popToViewController(controller, animated: true)
                                                                  break
                                                              }
                                                          }
                        }else{
                            
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                        }
                    }else{
                        
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                    }
                }
                
            }
        }else{
            
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }
    }
    
    
}
// MARK:
// MARK:- Validation

extension RSVPRejectVC {
    func validationForReject() -> Bool {
        if(txtDescription.text?.count == 0){
            
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_Reason, viewcontrol: self)
            
            return false
        }
        return true
    }
    
    
    
    
}

// MARK: - --------------UITextViewDelegate
// MARK: -
extension RSVPRejectVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        lblCount.text =  "\(newText.count)/250"
        return newText.count < 250
    }
    
}
