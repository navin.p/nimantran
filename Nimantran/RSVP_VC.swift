//
//  RSVP_VC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/25/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import Alamofire
import CRNotifications
import LoadingPlaceholderView


class RSVP_VC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var viewHold: UIView!
    @IBOutlet weak var txtSearch: UISearchBar!
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strErrorMessage  = ""
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    private var numberOfSections = 0
    private var numberOfRows = 0
    private var loadingPlaceholderView = LoadingPlaceholderView(){
        didSet{
            do {
                loadingPlaceholderView.gradientColor = .white
                loadingPlaceholderView.backgroundColor = .white
            }
        }
    }
    var refreshControl = UIRefreshControl()
    
    private var cellsIdentifiers = [
        "EventCell",
        "EventCell","EventCell",
        "EventCell","EventCell",
        "EventCell"
    ]
    
    
    // MARK:
        // MARK:- Life Cycle
     
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
              refreshControl.attributedTitle = NSAttributedString(string: "Get Event list...")
              refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
              if(isInternetAvailable()){
                  loadingPlaceholderView.cover(viewHold, animated: true)
                  getEventLiastByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
              }else{
                  self.aryList = NSMutableArray()
                  self.strErrorMessage = alertInternet
                  self.tableView.reloadData()
              }
              tableView.addSubview(refreshControl)
              
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          self.view.endEditing(true)
          
      }
      override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
      }
      
    // MARK:
       // MARK:- Extra function
       
       @objc func refresh(sender:AnyObject) {
           txtSearch.text = ""
           getEventLiastByUserID(userID: "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", tag: 0)
           
       }
   // MARK:
      // MARK:- IBAction
      
      @IBAction func actionOnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
    // MARK:
    // MARK:- API Calling
    
    func getEventLiastByUserID(userID : String , tag : Int) {

        let dict = NSMutableDictionary()
        dict.setValue("0", forKey: "EventId")
        dict.setValue("0", forKey: "UserId")
        dict.setValue("0", forKey: "AddressBookId")
        dict.setValue("0", forKey: "SenderByUserId")
        dict.setValue("\(userID)", forKey: "RecipientUserId")
        dict.setValue("True", forKey: "IsActive")
        
        //  var jsoneString = getJson(from: dict)
        
        WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GetEvent) { (responce, status) in
            
            if(status){
                let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                if(apiStatus == "True"){
                    let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                    print("----------\(arryData)")
                    
                    self.aryList = NSMutableArray()
                    self.aryList = arryData.mutableCopy()as! NSMutableArray
                    self.arySelectedList = NSMutableArray()
                    self.arySelectedList = arryData.mutableCopy()as! NSMutableArray
                    self.strErrorMessage = ""
                }else{
                    self.arySelectedList = NSMutableArray()
                    self.aryList = NSMutableArray()
                    self.strErrorMessage = apiError
                }
            }else{
                self.arySelectedList = NSMutableArray()
                self.aryList = NSMutableArray()
                self.strErrorMessage = alertSomeError
            }
            
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.loadingPlaceholderView.uncover(animated: true)
                self.numberOfSections = 1
                self.numberOfRows = self.arySelectedList.count
                self.tableView.reloadData()
            }
        }
        
        
    }
    
}
// MARK: -
// MARK: -UITableViewDelegate

extension RSVP_VC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "EventCell", for: indexPath as IndexPath) as! EventCell
        if(arySelectedList.count != 0){
            let dict = arySelectedList.object(at: indexPath.row)as! NSDictionary
            cell.lbl_EventTitle.text = "\(dict.value(forKey: "EventName")!)"
            cell.lbl_EventName.text = "\(dict.value(forKey: "AboutEvent")!)"
            cell.lbl_EventAddress.text = "\(dict.value(forKey: "EventAddress")!)"
            cell.lbl_EventDate.text = "Date: \(dict.value(forKey: "EventFromDate")!)"
            
            cell.btnContact.tag = indexPath.row
            cell.btnContact.addTarget(self, action: #selector(buttonCalling), for: .touchUpInside)
            cell.btnContact.setTitle("\(dict.value(forKey: "ContactNumber")!)", for: .normal)
        
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            
            
            cell.btnAccept.addTarget(self, action: #selector(buttonAccept), for: .touchUpInside)
            cell.btnReject.addTarget(self, action: #selector(buttonReject), for: .touchUpInside)
            
            
            cell.btnAccept.layer.cornerRadius = 8.0
            cell.btnReject.layer.cornerRadius = 8.0
            
            cell.btnAccept.layer.borderWidth = 1.0
            cell.btnReject.layer.borderWidth = 1.0
            
            cell.btnAccept.layer.borderColor = UIColor.lightGray.cgColor
            cell.btnReject.layer.borderColor = UIColor.lightGray.cgColor
            
            cell.lbl_EventStatic.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            cell.lbl_EventStatic.frame = CGRect(x: 0, y: 3, width: 15, height: cell.btnReject.frame.maxY)
            

        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(arySelectedList.count != 0){
          //  let vc = mainStoryboard.instantiateViewController(withIdentifier: "EventDetailVC") as? EventDetailVC
          //  vc?.dictEventData = (arySelectedList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
           // self.navigationController?.pushViewController(vc!, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }

    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
 
    @objc func buttonReject(_ sender: UIButton!) {
        print("Button tapped")
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "RSVPRejectVC") as? RSVPRejectVC
        vc?.dictEventData = (arySelectedList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func buttonAccept(_ sender: UIButton!) {
        print("Button tapped")
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "RSVPAcceptRejectVC") as? RSVPAcceptRejectVC
        vc?.dictEventData = (arySelectedList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func buttonCalling(_ sender: UIButton!) {
        
        if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
        }
        
        
    }
}


// MARK: -
// MARK: - UISearchBarDelegate


extension RSVP_VC: UISearchBarDelegate  {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
        self.view.endEditing(true)
    }
    
    
    
    func searchAutocomplete(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "EventName contains[c] %@ OR EventAddress contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
        }
        else{
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = self.aryList.mutableCopy() as! NSMutableArray
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(arySelectedList.count == 0){
            self.strErrorMessage = alertDataNotFound
            
        }
        
        DispatchQueue.main.async {
            self.numberOfSections = 1
            self.numberOfRows = self.arySelectedList.count
            self.tableView.reloadData()
        }
        
        
    }
}
