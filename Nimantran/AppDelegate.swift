//
//  AppDelegate.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseInstanceID
import UserNotifications
import FirebaseMessaging

import MessageUI


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let center = UNUserNotificationCenter.current()



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
              // overrideUserInterfaceStyle = .dark    
         if DeviceType.IS_IPAD {
                        mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                    }else{
                        mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                    }
        if #available(iOS 13.0, *) {
                  
        }else{
              gotoViewController()
        }
             //1 FireBase-----------
        registerForRemoteNotifications()
             FirebaseApp.configure()
             NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
             center.delegate = self
        
        return true
    }
    func gotoViewController()  {
        
        if (nsud.value(forKey: "Nim_KeepMeLogin") != nil) {
            
            if(nsud.value(forKey: "Nim_KeepMeLogin")as! Bool){
                let obj_ISVC : DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }else{
                let obj_ISVC : SignInVC = mainStoryboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }
           
        }else{
          let obj_ISVC : TutorialVC = mainStoryboard.instantiateViewController(withIdentifier: "TutorialVC") as! TutorialVC
             let navigationController = UINavigationController(rootViewController: obj_ISVC)
                                                   rootViewController(nav: navigationController)
        }
        
    }
    func rootViewController(nav : UINavigationController)  {
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: UISceneSession Lifecycle
@available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
@available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Nimantran")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

// MARK: --------UNNotification/Messaging Delegate Method----------

extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate{
    //MARK: RegisterPushNotification
    func registerForRemoteNotifications()  {
        if #available(iOS 10, *) {
            
            center.requestAuthorization(options: [.alert, .sound]) { granted, error in
                // Enable or disable features based on authorization.
            }
            center.delegate = self
            center.getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .authorized, .provisional:
                    print("authorized")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        Messaging.messaging().delegate = self
                    }
                    
                case .denied:break
                case .notDetermined:break
                @unknown default:
                    break
                }
            })
            
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    private func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
    
    
    
    func hendelNotidata(userInfo :NSDictionary){
        let obj_ISVC : NotificationVC = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        obj_ISVC.dictNotiData = userInfo.mutableCopy()as! NSMutableDictionary
        let navigationController = UINavigationController(rootViewController: obj_ISVC)
        rootViewController(nav: navigationController)
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        
        InstanceID.instanceID().instanceID { (result, error) in
        if let error = error {
        print("Error fetching remote instange ID: \(error)")
        } else if let result = result {
            nsud.setValue("\(result.token)", forKey: "NIM_FCM_Token")
            nsud.synchronize()

        print("Remote instance ID token: \(result.token)")
         }
        }
     
    }
    //Handle Local Notification Center Delegate methods
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge,.alert,.sound])
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        if state == .active {
         
        }
        else  {
            
        }
    }
    
    
    
    // application kill
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      //  FTIndicator.showToastMessage("HI notification AYAYA")
        let userInfo = response.notification.request.content.userInfo
        if userInfo.count == 0 {
          hendelNotidata(userInfo: userInfo as NSDictionary)
        }else{
      
        }
    }
    
    // Forground and background
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let state: UIApplication.State = UIApplication.shared.applicationState
      //  let strPushType = "\((userInfo as AnyObject).value(forKey: "PushType")!)"
     if state == .active { //
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NIM_Notification"), object: nil, userInfo: userInfo)
       }
                else   //  .Inactive (background to forground)
                {
                    hendelNotidata(userInfo: userInfo as NSDictionary)
                }
        
    }
    
    
}


