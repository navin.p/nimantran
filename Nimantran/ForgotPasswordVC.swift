//
//  ForgotPasswordVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class ForgotPasswordVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var btnSubmit: TransitionButton!
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    override func viewWillLayoutSubviews() {
        for item in self.btnSubmit.layer.sublayers! {
            if item is CAGradientLayer {
                item.removeFromSuperlayer()
            }
        }
        btnSubmit.layer.insertSublayer(getgradientLayer(bounds: btnSubmit.bounds), at: 0)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {

                     let text = textField.text ?? ""

                     let trimmedText = text.trimmingCharacters(in: .whitespaces)

                     textField.text = trimmedText
                if(txtMobileNumber == textField){
                  textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
                    textField.text = textField.text?.replacingOccurrences(of: " ", with: "")

              }
         }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: TransitionButton) {
        self.view.endEditing(true)
     self.navigationController?.popViewController(animated: true)
       // openWhatsapp()
    }
    
    @IBAction func actionOnSubmit(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(validationForForgotPass()){
            ForgotAPI(mobileNumber: txtMobileNumber.text!)
        }
        
    }
    func openWhatsapp(){
        let urlWhats = "whatsapp://send?phone=+918962488673,+919589063689,+918962488673&text=Discussion for Nimantran (demo), Mumma's Kitchen and App for GSITS alumni has to be done. Sir has called in cabin."
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    print("Install Whatsapp")
                }
            }
        }
    }
}
// MARK:
// MARK:- Validation

extension ForgotPasswordVC {
    func validationForForgotPass() -> Bool {
        if(txtMobileNumber.text?.count == 0){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumber)
            
            return false
        }else if(txtMobileNumber.text!.count < 10){
            
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumberValid)
            return false
            
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension ForgotPasswordVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK: -
// MARK: - API Calling


extension ForgotPasswordVC  {
    
    func ForgotAPI(mobileNumber : String) {
        if(isInternetAvailable()){
            self.btnSubmit.startAnimation()

            let urlForgotPass = "\(URL_ForgetPassword)MobileNumber=\(mobileNumber)"
            WebServiceClass.getRequestWithHeadersGET(url: urlForgotPass) { (responce, status) in
                if(status){
                    
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if(apiStatus == "True"){
                        
                        self.btnSubmit.stopAnimation(animationStyle: .normal, revertAfterDelay: 05) {
                           
                            
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else{
                        self.btnSubmit.stopAnimation(animationStyle: .shake, revertAfterDelay: 05) {
                               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }
                }else{
                    self.btnSubmit.stopAnimation(animationStyle: .shake, revertAfterDelay: 05) {
                         CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
        }
    }
    
}
