//
//  SignUpVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications
import Alamofire
class SignUpVC: UIViewController {
    
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var txtConfirmPassword: ACFloatingTextfield!
    @IBOutlet weak var btnSignUp: TransitionButton!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var btnEdit: UIButton!

    var strProfileName = ""
    var imagePicker = UIImagePickerController()

    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
    }
    
    
    override func viewWillLayoutSubviews() {
        for item in self.btnSignUp.layer.sublayers! {
            if item is CAGradientLayer {
                item.removeFromSuperlayer()
            }
        }
        btnSignUp.layer.insertSublayer(getgradientLayer(bounds: btnSignUp.bounds), at: 0)
        /// scroll.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 800)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {

                  let text = textField.text ?? ""

                  let trimmedText = text.trimmingCharacters(in: .whitespaces)

                  textField.text = trimmedText
             if(txtMobile == textField){
               textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
                textField.text = textField.text?.replacingOccurrences(of: " ", with: "")

           }
      }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnSignUP(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(validationForSignUP()){
            let dict = ["UserId": "0", "Name": "\(txtUserName.text!)", "MobileNumber": "\(txtMobile.text!)", "EmailId": "\(txtEmail.text!)", "Password": "\(txtPassword.text!)", "ProfileImage": "\(strProfileName)", "EventLimit": "1"]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(dict) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print(jsonString)
                    APIUserRegistration(dict: jsonString, imageProfile: self.imgProfile.image!, imageName: strProfileName)
                }
            }
        }
    }
    @IBAction func actionOnAcceptTermCheckBoX(_ sender: UIButton) {
        self.view.endEditing(true)
        
    }
    
    @IBAction func actionOnProfileImage(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let actioncamera = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction!) in
            
            self.imagePicker = UIImagePickerController()

                           if UIImagePickerController.isSourceTypeAvailable(.camera){
                               print("Button capture")
                               
                               self.imagePicker.delegate = self
                               self.imagePicker.sourceType = .camera
                               self.imagePicker.allowsEditing = false
                                self.present(self.imagePicker, animated: true, completion: nil)
                           }else{
                            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)

            }
        }
        actioncamera.setValue(UIImage(named: "camera-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        
        alertController.addAction(actioncamera)
        
        
        let actionGallery = UIAlertAction(title: "Gallery", style: .default) { (action: UIAlertAction!) in
            self.imagePicker = UIImagePickerController()
                           if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                               print("Button capture")
                               self.imagePicker.delegate = self
                               self.imagePicker.sourceType = .savedPhotosAlbum
                               self.imagePicker.allowsEditing = false
                                self.present(self.imagePicker, animated: true, completion: nil)
                           }else{
                            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)

            }
        }
        actionGallery.setValue(UIImage(named: "gallery-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        alertController.addAction(actionGallery)
        let actionDismiss = UIAlertAction(title: "Dismiss", style: .destructive) { (action: UIAlertAction!) in
            
         }
         alertController.addAction(actionDismiss)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func actionOnAcceptTerm(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "WebClass") as? WebClass
                  vc?.titleString = "Terms & Conditions"
                  vc?.urlString = URL_termsofuse
                  self.present((vc)!, animated: true) {
                  }
    }
    @IBAction func actionOnBack(_ sender: TransitionButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnCheckBox(_ sender: UIButton) {
        self.view.endEditing(true)
        if(btnCheckBox.currentImage == UIImage(named: "uncheck")){
            btnCheckBox.setImage(UIImage(named: "check"), for: .normal)
        }else{
            btnCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        }
    }
}
// MARK:
// MARK:- Validation

extension SignUpVC {
    func validationForSignUP() -> Bool {
        
        
        if(txtUserName.text?.count == 0){
            addErrorMessage(textView: txtUserName, message: alert_UserName)
            return false
        }
            
            
        else if(txtMobile.text?.count == 0){
            addErrorMessage(textView: txtMobile, message: alert_MobileNumber)
            
            return false
        }else if(txtMobile.text!.count < 10){
            
            addErrorMessage(textView: txtMobile, message: alert_MobileNumberValid)
            return false
            
        }
        else if (txtEmail.text?.count == 0){
            addErrorMessage(textView: txtEmail, message: alert_Email)
            return false
            
        }
            
        else if !(txtEmail.text!.isValidEmailAddress()){
            addErrorMessage(textView: txtEmail, message: alert_EmailValid)
            return false
            
        }
            
        else if(txtPassword.text!.count == 0){
            addErrorMessage(textView: txtPassword, message: alert_Password)
            return false
        }
        else if(txtConfirmPassword.text!.count == 0){
            addErrorMessage(textView: txtConfirmPassword, message: alert_CPassword)
            return false
        }
            
        else if(txtConfirmPassword.text! != txtPassword.text!){
            addErrorMessage(textView: txtConfirmPassword, message: alert_Password_CPassword)
            return false
        }
        else if(btnCheckBox.currentImage == UIImage(named: "uncheck")){
            CRNotifications.showNotification(type: CRNotifications.info, title: alertMessage, message: alert_Terms, dismissDelay: 3)
            return false
        }
        
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension SignUpVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

// MARK: -
// MARK: - API Calling


extension SignUpVC  {
    
    func APIUserRegistration(dict : String , imageProfile : UIImage , imageName : String) {
        if(isInternetAvailable()){
            self.btnSignUp.startAnimation()
            WebServiceClass.getRequestWithHeadersWithImage(JsonData: dict,JsonDataKey: "UserRegistrationMdl", image: imageProfile, imageName: imageName, url: URL_AddUpdateUserRegistration) { (responce, status) in
                if(status){
                    let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if(apiStatus == "True"){
                        self.btnSignUp.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
                 
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else{
                        self.btnSignUp.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                        }
                    }
                }else{
                    self.btnSignUp.stopAnimation(animationStyle: .shake, revertAfterDelay: 01) {
                         CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }
            }
          
            
        }
        else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
            
        }
    }
}
// MARK: -
// MARK: -UINavigationControllerDelegate
extension SignUpVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.strProfileName = getUniqueString() + ".png"
        self.imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
          
           
    }
}
