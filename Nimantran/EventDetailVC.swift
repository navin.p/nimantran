//
//  EventDetailVC.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/17/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import MessageUI
import UserNotifications
import CRNotifications
import LoadingPlaceholderView
import TransitionButton

class EventDetailVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventSubDetail: UILabel!
    
    @IBOutlet weak var lblEventDate: UILabel!
    
    @IBOutlet weak var lblEventAddress: UILabel!
    @IBOutlet weak var lblEventContact: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnNext: TransitionButton!

    // MARK:
    // MARK:- var
    var ary_CollectionData = NSMutableArray()
    var dictEventData = NSMutableDictionary()
    var aryRecipientList = NSMutableArray()
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictEventData)
        dictEventData = dictEventData.removeNullFromDict()
        lblTitle.text = "\(dictEventData.value(forKey: "EventName")!)"
        lblEventName.text = "\(dictEventData.value(forKey: "EventName")!)"
        lblEventSubDetail.text = "\(dictEventData.value(forKey: "AboutEvent")!)"
        lblEventDate.text = "\(dictEventData.value(forKey: "EventFromDate")!)"
        
        lblEventAddress.text = "\(dictEventData.value(forKey: "EventAddress")!)"
        lblEventContact.text = "\(dictEventData.value(forKey: "ContactNumber")!)"
        
        let imagesEvent = "\(dictEventData.value(forKey: "EventImages")!)"
        let aryTemp = imagesEvent.split(separator: ",")
        ary_CollectionData = ((aryTemp as AnyObject)as! NSArray).mutableCopy() as! NSMutableArray
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
        
    }
    override func viewWillLayoutSubviews() {
           for item in self.btnNext.layer.sublayers! {
               if item is CAGradientLayer {
                   item.removeFromSuperlayer()
               }
           }
           btnNext.layer.insertSublayer(getgradientLayer(bounds: btnNext.bounds), at: 0)
       }
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionGoCO_HostList(_ sender: UIButton) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CohostListVC") as? CohostListVC
        vc!.dictEventData = self.dictEventData
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    @IBAction func actionOnScan(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "QRScannerController")as! QRScannerController
               testController.viewComeFrom = ""
        testController.dictEventData = self.dictEventData
        
               self.navigationController?.pushViewController(testController, animated: true)
       }

    
    @IBAction func actionOnSendInvite(_ sender: UIButton) {
        
        self.view.endEditing(true)
         print(aryRecipientList)
        
        if(self.aryRecipientList.count != 0){
            
          openNextController()
            
        }else{
            
            let strLoginUserID = "\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)"
                             
                             let strEventUserID =  "\(dictEventData.value(forKey: "EventId")!)"
                             
                             let dict = NSMutableDictionary()
                                       dict.setValue("\(dictEventData.value(forKey: "EventId")!)", forKey: "EventId")
                             if(strLoginUserID == strEventUserID){
                                 dict.setValue("0", forKey: "InviteByUserId")

                             }else{
                                 dict.setValue("\(getLogInData(key: "Nim_LoginData").value(forKey: "UserId")!)", forKey: "InviteByUserId")

                             }
                                       
                             
                             getGuestListByUserID(dict: dict, tag: 0)
            
        }
    }
    
    func openNextController()  {
        
        if(self.aryRecipientList.count != 0){
                   
                          let alert = UIAlertController(title: "", message: alert_SMS,preferredStyle: .actionSheet)
                                
                                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
                               
                                      let arySend = NSMutableArray()
                                   for item in self.aryRecipientList {
                                              let dict = NSMutableDictionary()
                                              dict.setValue("\((item as AnyObject).value(forKey: "EventNimantranId")!)", forKey: "EventNimantranId")
                                              arySend.add(dict)
                                          }
                                   self.APIAddRequestCount(Jsondict: arySend)
                                }))
                                alert.addAction(UIAlertAction(title: "Cancel",
                                                              style: UIAlertAction.Style.destructive,
                                                              handler: {(_: UIAlertAction!) in
                                                                //Sign out action
                                }))
                                self.present(alert, animated: true, completion: nil)
                   
               }else{
                   
              CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertRecipientAdd, dismissDelay: 2)

                   
               }
        

    }
    // MARK:
    // MARK:- Calling API

    
      func getGuestListByUserID(dict : NSDictionary , tag : Int) {
          if(isInternetAvailable()){
            
            self.btnNext.startAnimation()

               WebServiceClass.getRequestWithHeaders(dictJson: dict, url: URL_GetEventAddressBookNimantran) { (responce, status) in
                self.btnNext.stopAnimation()
             if(status){
                      let apiStatus = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Result")!)"
                      let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      if(apiStatus == "True"){
                         
                        let arryData = (responce.value(forKey: "data")as! NSDictionary).value(forKey: "DTList")as! NSArray
                                           self.aryRecipientList = NSMutableArray()
                                           self.aryRecipientList = arryData.mutableCopy()as! NSMutableArray
                        self.openNextController()
                  
                      }else{
                          CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 2)
                      }
                  }else{
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)

                  }
              }
          }else{
          CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)

          }
      }
    
    func APIAddRequestCount(Jsondict : NSArray) {
         if(isInternetAvailable()){
           self.btnNext.startAnimation()
             WebServiceClass.getRequestWithHeadersWithArray(JsonArray: Jsondict, url: URL_UpdateSentInvitationCount) { (responce, status) in
                 
                 self.btnNext.stopAnimation(animationStyle: .normal, revertAfterDelay: 01) {
                     if(status){
                         
                         let resultData = (responce.value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary
                         
                         let apiStatus = "\(resultData.value(forKey: "Result")!)"
                         let apiError = "\(resultData.value(forKey: "Success")!)"
                         if(apiStatus == "True"){
                             
                            
                            self.openMessageComposser()
                         }else{
                             CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                         }
                     }else{
                         CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                     }
                 }
             }
         }
         else{
             CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
             
         }
     }
    
   
    func openMessageComposser() {
           if (MFMessageComposeViewController.canSendText()) {
                                   let controller = MFMessageComposeViewController()
                                   controller.body = "Message Body"
                                   
                                   var strNumber = ""
                                   for item in self.aryRecipientList {
                                       strNumber = (item as AnyObject).value(forKey: "MobileNumber") as! String + ","
                                   }
                                   if(strNumber.count != 0){
                                       strNumber = String(String(strNumber).dropLast())
                                   }
                                   let recipientsArray = strNumber.components(separatedBy: ",")
                                   controller.recipients = recipientsArray
                                   controller.messageComposeDelegate = self
                                   self.present(controller, animated: true, completion: nil)
           }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertCalling, dismissDelay: 3)

        }
    }
    
}

// MARK:
// MARK:- UICollectionView
extension EventDetailVC : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
}

// MARK:
// MARK:- UICollectionView
extension EventDetailVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath as IndexPath) as! eventCell
        
        let urlImage = "\(URL_eventimagesDownLoad)\(ary_CollectionData[indexPath.row]as! String)"
        cell.event_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            //   print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        cell.event_Image.layer.cornerRadius = 5
        cell.event_Image.contentMode = .scaleToFill
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height:100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    
}
class eventCell: UICollectionViewCell {
    @IBOutlet weak var event_Image: UIImageView!
    
    
}
